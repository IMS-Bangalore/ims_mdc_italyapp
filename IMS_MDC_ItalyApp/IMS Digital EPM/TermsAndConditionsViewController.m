//
//  TermsAndConditionsViewController.m
//  IMS Digital EPM
//
//  Created by Diego Prados on 21/05/13.
//
//

#import "TermsAndConditionsViewController.h"

@interface TermsAndConditionsViewController ()

@end

@implementation TermsAndConditionsViewController

#pragma mark - Init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithDelegate:(id<TermsAndConditionsViewControllerDelegate>)delegate {
    self = [super initWithNibName:@"TermsAndConditionsViewController" bundle:nil];
    if (self) {
        _delegate = delegate;
    }
    return self;
}

- (IBAction)acceptTerms:(id)sender {
    [self.delegate didAcceptTerms];
}

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_termsTextView release];
    [_acceptTermsMessageLabel release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setTermsTextView:nil];
    [self setAcceptTermsMessageLabel:nil];
    [super viewDidUnload];
}

@end
