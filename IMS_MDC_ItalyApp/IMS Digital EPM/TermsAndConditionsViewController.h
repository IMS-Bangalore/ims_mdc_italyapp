//
//  TermsAndConditionsViewController.h
//  IMS Digital EPM
//
//  Created by Diego Prados on 21/05/13.
//
//

#import <UIKit/UIKit.h>

@protocol TermsAndConditionsViewControllerDelegate <NSObject>

- (void)didAcceptTerms;

@end

@interface TermsAndConditionsViewController : UIViewController

#pragma mark - Properties
@property (nonatomic, assign) id<TermsAndConditionsViewControllerDelegate> delegate;

#pragma mark - IBOutlets
@property (retain, nonatomic) IBOutlet UITextView *termsTextView;
@property (retain, nonatomic) IBOutlet UILabel *acceptTermsMessageLabel;

#pragma mark - Public methods
- (id)initWithDelegate:(id<TermsAndConditionsViewControllerDelegate>)delegate;

#pragma mark - IBActions
- (IBAction)acceptTerms:(id)sender;

@end
