//
//  PopUpMensajeViewController.h
//  IMS Digital EPM
//
//  Created by Ricardo Berzal on 30/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

enum {
    kPopUpMensajeCancelButton = 0,
    kPopUpMensajeOKButton = 1
};

@protocol PopUpMensajeViewControllerProtocol
@required
- (void) CierreModalPopUpMensaje:(id)sender;
@end

@interface PopUpMensajeViewController : UIViewController {
    NSObject<PopUpMensajeViewControllerProtocol>* _delegate;  //objeto delegado
    NSString* _msg;
    int _tipo;
}
@property (readonly) NSInteger Seleccionada;  //boton pulsado

@property (retain, nonatomic) IBOutlet UIButton *BotonSI;
@property (retain, nonatomic) IBOutlet UIButton *BotonNO;
@property (retain, nonatomic) IBOutlet UILabel *LTexto;

- (IBAction)TouchUpInsideBoton:(id)sender;


//delegate: objeto delegado (nil si ninguno)
//tipo: 0: dos botones; 1: 1 botón; 2: sin botones y en rojo
//msg: texto de mensaje 
- (PopUpMensajeViewController*)initWithDelegate: (NSObject<PopUpMensajeViewControllerProtocol>*) delegate Tipo:(int)Tipo msg:(NSString*)msg;

@end
