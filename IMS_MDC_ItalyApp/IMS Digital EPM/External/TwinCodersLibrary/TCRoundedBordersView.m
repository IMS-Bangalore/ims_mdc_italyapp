//
//  RoundedBordersView.m
//  TwinCodersLibrary
//
//  Created by Guillermo Gutiérrez on 09/05/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "TCRoundedBordersView.h"
#import <QuartzCore/QuartzCore.h>

static const CGFloat kDefaultCornerRadius = 7;

@implementation TCRoundedBordersView
@synthesize cornerRadius = _cornerRadius;

- (void)reloadCorners {
    self.layer.masksToBounds = YES;
    self.layer.cornerRadius = self.cornerRadius.floatValue;
}

- (void)setCornerRadius:(NSNumber*)cornerRadius {
    _cornerRadius = cornerRadius;
    [self reloadCorners];
}

- (NSNumber *)cornerRadius {
    if (_cornerRadius == nil) {
        _cornerRadius = [NSNumber numberWithFloat:kDefaultCornerRadius];
    }
    return _cornerRadius;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self reloadCorners];
}

@end
