//
//  TCNinePatchImageView.m
//  FnacSocios
//
//  Created by Guillermo Gutiérrez on 13/04/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "TCNinePatchImageView.h"
#import "UIImage+NinePatch.h"

@implementation TCNinePatchImageView

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (( self = [super initWithCoder:aDecoder] )) {
        UIImage* image = [self image];
        if (image != nil) {
            [super setImage:[image ninePatchImage]];
        }
        UIImage* highlightedImage = [self highlightedImage];
        if (highlightedImage != nil) {
            [super setHighlightedImage:[highlightedImage ninePatchImage]];
        }
    }
    return self;
}

- (void)setImage:(UIImage *)image {
    [super setImage:[image ninePatchImage]];
}

- (void)setHighlightedImage:(UIImage *)highlightedImage {
    [super setHighlightedImage:[highlightedImage ninePatchImage]];
}

@end
