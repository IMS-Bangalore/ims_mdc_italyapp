//
//  TCReusableSelectionView.h
//  TwinCodersLibrary
//
//  Created by Guillermo Gutiérrez on 16/01/13.
//  Copyright (c) 2013 TwinCoders S.L. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol TCReusableSelectionView <NSObject>
@property (nonatomic, readonly) NSString* reuseIdentifier;
@end
