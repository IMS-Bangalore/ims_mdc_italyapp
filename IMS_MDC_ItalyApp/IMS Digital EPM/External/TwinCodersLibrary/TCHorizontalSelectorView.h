//
//  TCHorizontalSelectorView.h
//  TwinCodersLibrary
//
//  Created by Guillermo Gutiérrez on 16/01/13.
//  Copyright (c) 2013 TwinCoders S.L. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TCCustomPageControl.h"
#import "TCReusableSelectionView.h"


#pragma mark Type definitions
typedef enum {
    TCHorizontalSelectorTypeCenter = 0,
    TCHorizontalSelectorTypeFill
} TCHorizontalSelectorType;

typedef UIView* (^TCHorizontalSelectorDataSource)(NSInteger elementIndex);
typedef void (^TCHorizontalSelectorElementSelected)(NSInteger elementIndex);

#pragma mark Public interface
@interface TCHorizontalSelectorView : UIView<UIScrollViewDelegate, TCCustomPageControlDelegate>

#pragma mark - Properties
@property (nonatomic, getter=isNotifyOnClick) BOOL notifyOnClick; // If true, the selection block will be called when the product view is clicked instead of when is shown in the scroll. Only works for TCClickableUIViews or UIButton subclasses
@property (nonatomic, getter=isNotifyOnScrollStop) BOOL notifyOnScrollStop; // If true, the selection block will be called when the scroll completely stops instead of when the page changes

@property (nonatomic) NSInteger elementsPerPage; // Specifies the number of elements shown on each page. If set to a number different than one and notifyOnClick is disabled, selection will be notified for the first element of each page

@property (nonatomic) TCHorizontalSelectorType type;
@property (nonatomic) BOOL circularScrollEnabled;
@property (nonatomic, strong) NSNumber* preRenderedElements; // Indicates the amount of elements to pre-render front and behind the current visible elements. Defaults to 0.
@property (nonatomic) BOOL removeElementsOutOfBounds; // Removes elements out of the scroll bounds so they can be reused. Defaults to YES

@property (nonatomic) NSInteger selectedElement;
@property (nonatomic, readonly) NSInteger numberOfElements;
@property (nonatomic, readonly) NSInteger numberOfPages;

#pragma mark - IBOutlets
@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) IBOutlet TCCustomPageControl *pageControl;

#pragma mark - Public methods
- (UIView<TCReusableSelectionView>*)dequeueViewWithReusableIdentifier:(NSString*)reuseIdentifier;
- (void)setNumberOfElements:(NSInteger)numberOfElements
             withDataSource:(TCHorizontalSelectorDataSource)dataSource
          onElementSelected:(TCHorizontalSelectorElementSelected)onElementSelected;

- (void)setSelectedElement:(NSInteger)selectedElement animated:(BOOL)animated;

- (void)resetViews;

@end
