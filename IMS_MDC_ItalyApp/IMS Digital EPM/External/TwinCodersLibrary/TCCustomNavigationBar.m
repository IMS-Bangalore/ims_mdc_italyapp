//
//  TCCustomNavigationBar.m
//  TwinCodersLibrary
//
//  Created by Guillermo Gutiérrez on 13/06/11.
//  Copyright TwinCoders All rights reserved.
//

#import "TCCustomNavigationBar.h"


@implementation TCCustomNavigationBar

- (void)setCustomImage:(UIImage *)customImage {
    if (_customImage != customImage) {
        _customImage = customImage;
        [self setNeedsDisplay];
    }
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    [_customImage drawInRect:self.bounds];
}

@end
