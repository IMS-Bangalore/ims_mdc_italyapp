//
//  TCRESTJSONRequest.m
//  TwinCodersLibrary
//
//  Created by Alex Gutiérrez on 16/11/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "TCRESTJSONRequest.h"
#import "TCRequestParam.h"
#import "JSONKit.h"

@implementation TCRESTJSONRequest

#pragma mark - Response handle

-(NSDictionary*) dictionaryForResponseString:(NSString*)string {
    NSDictionary* dictionary = [string objectFromJSONString];
    return dictionary;
}

#pragma mark - Serialization

-(NSString*) createBodyContent {
    NSString* bodyContent = nil;
    if (self.contentParams.count > 0) {
        NSDictionary* paramsDictionary = [self dictionaryForParams:self.contentParams];
        bodyContent = [paramsDictionary JSONString];
    }
    return bodyContent;
}

-(NSDictionary*) dictionaryForParams:(NSArray*)params {
    NSMutableDictionary* dictionary = [NSMutableDictionary dictionaryWithCapacity:params.count];
    for (TCRequestParam* param in params) {
        if (!param.isArray) {
            if (!param.isComplex) {
                dictionary[param.key] = param.value;
            } else {
                dictionary[param.key] = [self dictionaryForParams:param.params];
            }
        } else {
            dictionary[param.key] = param.params;
        }
    }
    return dictionary;
}

@end
