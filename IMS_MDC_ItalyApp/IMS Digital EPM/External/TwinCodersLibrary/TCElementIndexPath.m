//
//  TCPageIndexPath.m
//  TwinCodersLibrary
//
//  Created by Guillermo Gutiérrez on 16/01/13.
//  Copyright (c) 2013 TwinCoders S.L. All rights reserved.
//

#import "TCElementIndexPath.h"

@implementation TCElementIndexPath

- (id)initWithContentIndex:(NSInteger)contentIndex elementIndex:(NSInteger)elementIndex {
    if (( self = [super init] )) {
        self.elementIndex = elementIndex;
        self.contentIndex = contentIndex;
    }
    return self;
}

+ (id)elementIndexWithContentIndex:(NSInteger)contentIndex elementIndex:(NSInteger)elementIndex {
    return [[self alloc] initWithContentIndex:contentIndex elementIndex:elementIndex];
}

- (BOOL)isEqual:(id)object {
    return [object isKindOfClass:[TCElementIndexPath class]]
        && self.elementIndex == [(TCElementIndexPath*)object elementIndex]
        && self.contentIndex == [(TCElementIndexPath*)object contentIndex];
}

- (NSUInteger)hash {
    return _contentIndex * 10000 + _elementIndex;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"[%i, %i]", _contentIndex, _elementIndex];
}

- (NSString *)debugDescription {
    return self.description;
}

- (id)copyWithZone:(NSZone *)zone {
    return [[self class] elementIndexWithContentIndex:_contentIndex elementIndex:_elementIndex];
}



@end
