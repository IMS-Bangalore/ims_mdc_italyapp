//
//  TCCheckbox.h
//  TwinCodersLibrary
//
//  Created by Guillermo Gutiérrez on 13/06/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class TCCheckbox;
@protocol TCCheckBoxDelegate <NSObject>

-(void) checkBoxDidChangeValue:(TCCheckbox*)checkBox;

@end

@interface TCCheckbox : UIButton

@property (nonatomic, assign) IBOutlet id<TCCheckBoxDelegate> checkBoxDelegate;

@end
