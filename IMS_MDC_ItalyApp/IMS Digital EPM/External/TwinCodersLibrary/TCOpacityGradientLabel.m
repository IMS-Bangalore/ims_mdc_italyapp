//
//  TCOpacityGradientLabel.m
//  TwinCodersLibrary
//
//  Created by Guillermo Gutiérrez on 04/06/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "TCOpacityGradientLabel.h"
#import <QuartzCore/QuartzCore.h>


@interface TCOpacityGradientLabel ()
@property (nonatomic, strong) CALayer* gradientMaskLayer;
- (void)applyMaskIfNeeded;
@end

@implementation TCOpacityGradientLabel

#pragma mark - Init and dealloc
- (id)initWithCoder:(NSCoder *)aDecoder {
    if (( self = [super initWithCoder:aDecoder] )) {
        [self applyMaskIfNeeded];
    }
    return self;
}

- (NSNumber *)gradientStartPoint {
    if (_gradientStartPoint == nil) {
        _gradientStartPoint = [NSNumber numberWithFloat:0.7];
    }
    return _gradientStartPoint;
}

- (CALayer *)gradientMaskLayer {
    if (_gradientMaskLayer == nil) {
        CAGradientLayer* layer = [[CAGradientLayer alloc] init];
        layer.colors = [NSArray arrayWithObjects:(id)[[UIColor blackColor] CGColor], (id)[[UIColor clearColor] CGColor], nil];
        layer.locations = [NSArray arrayWithObjects:[NSNumber numberWithFloat:self.gradientStartPoint.floatValue], [NSNumber numberWithFloat:1], nil];
        layer.startPoint = CGPointMake(0, 0.5);
        layer.endPoint = CGPointMake(1.0, 0.5);
        
        self.gradientMaskLayer = layer;
    }
    _gradientMaskLayer.frame = CGRectMake(0, 0, self.layer.bounds.size.width, self.layer.bounds.size.height);
    return _gradientMaskLayer;
}

- (void)applyMaskIfNeeded {
    self.lineBreakMode = UILineBreakModeCharacterWrap; // Ignore iOS 6 deprecation
    UIFont* textFont = self.font;
    if ([self adjustsFontSizeToFitWidth]) {
        textFont = [textFont fontWithSize:[self minimumFontSize]]; // Ignore iOS 6 deprecation
    }
    
    CGSize textSize = [self.text sizeWithFont:textFont];
    if (textSize.width > self.frame.size.width) {
        self.layer.mask = self.gradientMaskLayer;
    }
    else {
        self.layer.mask = nil;
    }
}

- (void)setText:(NSString *)text {
    [super setText:text];
    
    [self applyMaskIfNeeded];
}

- (void)setFrame:(CGRect)frame {
    [super setFrame:frame];
    self.text = self.text;
}

@end
