//
//  TCModalLoadingView.h
//  TwinCodersLibrary
//
//  Created by Alex Guti on 22/06/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TCModalLoadingView : UIView

@property (nonatomic, assign, getter = isShowing) BOOL showing;

@property (strong, nonatomic) IBOutlet UILabel *textLabel;
@property (strong, nonatomic) IBOutlet UIView *contentView;

- (void)showLoadingViewWithText:(NSString*)text inView:(UIView*)view animated:(BOOL)animated;

- (void)dismiss:(BOOL)animated;

// Moves the current alert to the view
- (void)moveToView:(UIView*)view;

@end
