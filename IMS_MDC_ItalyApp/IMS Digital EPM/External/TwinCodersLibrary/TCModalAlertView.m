//
//  TCModalAlertView.m
//  TwinCodersLibrary
//
//  Created by Guillermo Gutiérrez on 14/05/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "TCModalAlertView.h"
#import "UIView+TCSuperView.h"
#import "TCAlertView.h"

static const CGFloat kFadeDuration = 0.2;
static const CGFloat kReboundDuration = 0.3;
static const NSInteger kTCAlertAppearanceCustomImage = -1;

@interface TCModalAlertView ()
@property (nonatomic, copy) TCAlertEventBlock onEventBlock;
@property (nonatomic, assign) CGRect singleButtonFrame;
@property (nonatomic, assign) CGRect acceptButtonFrame;
@property (nonatomic, assign) CGRect cancelButtonFrame;
@property (nonatomic, assign) CGFloat titleOneLineHeight;
@property (nonatomic, assign) CGFloat descriptionOneLineHeight;
@property (nonatomic, assign) CGFloat containerVsLabelOffset;
@property (nonatomic, assign, getter=isCustomModal) BOOL customModal;
@property (nonatomic, strong) TCAlertView* alertView;
@end

@implementation TCModalAlertView

#pragma mark - Init and dealloc
- (void)awakeFromNib {
    [super awakeFromNib];
    _singleButtonFrame = _singleButtonPlaceholderView.frame;
    _acceptButtonFrame = _acceptButton.frame;
    _cancelButtonFrame = _cancelButton.frame;
    
    _titleOneLineHeight = _titleLabel.frame.size.height;
    _descriptionOneLineHeight = _descriptionLabel.frame.size.height;
    _containerVsLabelOffset = _contentView.frame.size.height - _labelsPlaceholder.frame.size.height;
    self.customModal = YES;
}

- (id)init {
    if (( self = [super init] )) {
        self.customModal = NO;
    }
    return self;
}

#pragma mark - Private methods
- (void)adjustLabelsFrame {
    CGFloat titleHeight = 0;
    if (_titleLabel.text != nil) {
        CGSize titleSize = [_titleLabel.text sizeWithFont:_titleLabel.font constrainedToSize:CGSizeMake(_titleLabel.frame.size.width, self.frame.size.height) lineBreakMode:_titleLabel.lineBreakMode];
        titleHeight = titleSize.height - [_titleLabel.font lineHeight] + _titleOneLineHeight;
    }
    
    CGFloat descriptionHeight = 0;
    if (_descriptionLabel.text != nil) {
        CGSize descriptionSize = [_descriptionLabel.text sizeWithFont:_descriptionLabel.font constrainedToSize:CGSizeMake(_descriptionLabel.frame.size.width, self.frame.size.height) lineBreakMode:_descriptionLabel.lineBreakMode];
        descriptionHeight = descriptionSize.height - [_descriptionLabel.font lineHeight] + _descriptionOneLineHeight;
    }
    
    CGRect popupFrame = _contentView.frame;
    popupFrame.size.height = titleHeight + descriptionHeight + _containerVsLabelOffset;
    _contentView.frame = popupFrame;
    
    CGRect titleFrame = _titleLabel.frame;
    titleFrame.size.height = titleHeight;
    titleFrame.origin.y = 0;
    _titleLabel.frame = titleFrame;
    
    CGRect descriptionFrame = _descriptionLabel.frame;
    descriptionFrame.size.height = descriptionHeight;
    descriptionFrame.origin.y = titleHeight;
    _descriptionLabel.frame = descriptionFrame;
}

#pragma mark - Public methods
- (void)showAlertWithTitle:(NSString*)title
               description:(NSString*)description
         acceptButtonTitle:(NSString*)okButtonTitle
         cancelButtonTitle:(NSString*)cancelButtonTitle
           alertAppearance:(TCAlertAppearance)alertAppearance
                    inView:(UIView*)view
                  animated:(BOOL)animated
                   onEvent:(TCAlertEventBlock)onEvent {
    if ([self isShowing]) {
        TCLog(@"Already showing an alert dialog, canceling previous dialog");
        if (_onEventBlock != nil) {
            _onEventBlock(NO);
        }
        [self dismiss:NO];
    }
    
    self.showing = YES;

    self.onEventBlock = onEvent;
    
    if ([self isCustomModal]) {
        // Assign texts
        _titleLabel.text = title;
        _descriptionLabel.text = description;
        _acceptButton.hidden = okButtonTitle == nil;
        if (okButtonTitle != nil) {
            [_acceptButton setTitle:okButtonTitle forState:UIControlStateNormal];
        }
        _cancelButton.hidden = cancelButtonTitle == nil;
        if (cancelButtonTitle != nil) {
            [_cancelButton setTitle:cancelButtonTitle forState:UIControlStateNormal];
        }
        
        // Adjust button positions
        if (okButtonTitle == nil || cancelButtonTitle == nil) {
            _cancelButton.frame = _singleButtonFrame;
            _acceptButton.frame = _singleButtonFrame;
        }
        else {
            _cancelButton.frame = _cancelButtonFrame;
            _acceptButton.frame = _acceptButtonFrame;
        }
        
        // Adjust appearance
        _errorIconImageView.hidden = alertAppearance != kTCAlertAppearanceError;
        _infoIconImageView.hidden = alertAppearance != kTCAlertAppearanceInfo;
        _customIconImageView.hidden = alertAppearance != kTCAlertAppearanceCustomImage;
        
        [self adjustLabelsFrame];
        
        // Hide keyboard
        [[[UIApplication sharedApplication] keyWindow] endEditing:NO];
        
        // Show the view
        [view addSubview:self adjust:YES animated:animated];
    }
    else {
        // Use standard alert view
        self.alertView = [TCAlertView alertViewWithTitle:title message:description cancelButtonTitle:cancelButtonTitle otherButtonTitles:okButtonTitle != nil ? [NSArray arrayWithObject:okButtonTitle] : nil onDismiss:^(int buttonIndex) {
            onEvent(YES);
        } onCancel:^{
            onEvent(NO);
        }];
        [_alertView show];
    }
}

- (void)showAlertWithTitle:(NSString*)title
               description:(NSString*)description
         acceptButtonTitle:(NSString*)okButtonTitle
         cancelButtonTitle:(NSString*)cancelButtonTitle
                      icon:(UIImage*)iconImage
                    inView:(UIView*)view
                  animated:(BOOL)animated
                   onEvent:(TCAlertEventBlock)onEvent {
    self.customIconImageView.image = iconImage;
    [self showAlertWithTitle:title
                 description:description
           acceptButtonTitle:okButtonTitle
           cancelButtonTitle:cancelButtonTitle
             alertAppearance:kTCAlertAppearanceCustomImage
                      inView:view
                    animated:animated
                     onEvent:onEvent];
}

- (void)dismiss:(BOOL)animated {
    self.onEventBlock = nil;
    [self removeFromSuperviewAnimated:animated];
}

- (void)moveToView:(UIView *)view {
    if ([self isShowing]) {
        [self removeFromSuperviewAnimated:NO];
        [view addSubview:self adjust:YES animated:NO];
    }
}

#pragma mark - IBActions
- (IBAction)buttonClicked:(id)sender {
    TCAlertEventBlock block = [_onEventBlock copy];
    BOOL accepted = sender == _acceptButton;
    
    [self dismiss:YES];
    if (block != nil) {
        block(accepted);
    }
}


@end
