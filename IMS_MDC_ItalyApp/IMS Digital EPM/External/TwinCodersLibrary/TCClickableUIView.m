//
//  TCClickableUIView.m
//  TwinCodersLibrary
//
//  Created by Guillermo Gutiérrez Doral on 26/09/11.
//  Copyright TwinCoders All rights reserved.
//

#import "TCClickableUIView.h"
#import <QuartzCore/QuartzCore.h>

static const NSTimeInterval kAnimationDuration = 0.3;

@interface TCClickableUIView()
- (void)addObserverToButtonEvents;
@end

@implementation TCClickableUIView


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _enabled = YES;
        [self addObserverToButtonEvents];
    }
    return self;
}

- (id)init {
    self = [super init];
    if (self) {
        _enabled = YES;
        [self addObserverToButtonEvents];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        _enabled = YES;
        [self addObserverToButtonEvents];
    }
    return self;
}

- (void)addObserverToButtonEvents {
    [_actionButton addTarget:self action:@selector(buttonDown:) forControlEvents:UIControlEventTouchDown];
    [_actionButton addTarget:self action:@selector(buttonDown:) forControlEvents:UIControlEventTouchDragEnter];
    [_actionButton addTarget:self action:@selector(buttonUp:) forControlEvents:UIControlEventTouchUpInside];
    [_actionButton addTarget:self action:@selector(buttonUp:) forControlEvents:UIControlEventTouchDragExit];
    [_actionButton addTarget:self action:@selector(buttonUp:) forControlEvents:UIControlEventTouchCancel];
    [_actionButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)removeObserverForButtonEvents {
    [_actionButton removeTarget:self action:@selector(buttonDown:) forControlEvents:UIControlEventTouchDown];
    [_actionButton removeTarget:self action:@selector(buttonDown:) forControlEvents:UIControlEventTouchDragEnter];
    [_actionButton removeTarget:self action:@selector(buttonUp:) forControlEvents:UIControlEventTouchUpInside];
    [_actionButton removeTarget:self action:@selector(buttonUp:) forControlEvents:UIControlEventTouchDragExit];
    [_actionButton removeTarget:self action:@selector(buttonUp:) forControlEvents:UIControlEventTouchCancel];
    [_actionButton removeTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];    
}

- (void)setActionButton:(UIButton *)anActionButton
{
    if (_actionButton != anActionButton) {
        [self removeObserverForButtonEvents];
        
        _actionButton = anActionButton;
        
        [self addObserverToButtonEvents];
    }
}

- (void)highlightSubviews {
    for (id view in _actionHighlightElements) {
        if ([view respondsToSelector:@selector(setHighlighted:)]) {
            [view setHighlighted:_highlighted || _selected];
        }
    }
}

- (void)setEnabled:(BOOL)enabled {
    _enabled = enabled;
    self.alpha = enabled ? 1.0 : 0.5;
    self.userInteractionEnabled = enabled;
}

-(void)setSelected:(BOOL)selected {
    _selected = selected;
    [self highlightSubviews];
}

- (void)setHighlighted:(BOOL)highlighted {
    _highlighted = highlighted;
    [self highlightSubviews];
}

#pragma mark - Public methods
- (void)setEnabled:(BOOL)enabled animated:(BOOL)animated {
    if (animated) {
        [UIView animateWithDuration:kAnimationDuration animations:^{
            self.enabled = enabled;
        }];
    }
    else {
        self.enabled = enabled;
    }
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    if (animated) {
        CATransition* transition = [CATransition animation];
        transition.type = kCATransitionFade;
        transition.duration = kAnimationDuration;
        [self.layer addAnimation:transition forKey:@"highlightAnimation"];
    }
    self.highlighted = highlighted;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    if (animated) {
        CATransition* transition = [CATransition animation];
        transition.type = kCATransitionFade;
        transition.duration = kAnimationDuration;
        [self.layer addAnimation:transition forKey:@"selectedAnimation"];
    }
    self.selected = selected;
}

#pragma mark - Button events
- (void)buttonDown:(UIButton*)button {
    if (_actionButton == button) {
        self.highlighted = YES;
    }
}

- (void)buttonUp:(UIButton*)button {
    if (_actionButton == button) {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            self.highlighted = NO;
        }];
    }
}

- (void)buttonClicked:(UIButton*)button {
    if (_actionButton == button && _target != nil && _action != nil) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [_target performSelector:_action withObject:self];
#pragma clang diagnostic pop
    }
}


@end
