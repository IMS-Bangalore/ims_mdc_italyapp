//
//  TCCodeDrawView.m
//  SantanderChile
//
//  Created by Guillermo Gutiérrez on 28/10/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "TCCodeDrawView.h"
#import "UIImage+BBlock.h"
#import <QuartzCore/QuartzCore.h>

static const NSTimeInterval kAnimationDuration = 0.3;

@interface TCCodeDrawView()
@property (nonatomic, strong) UIImageView* contentImageView;
@end

@implementation TCCodeDrawView

#pragma mark - Init
- (id)init {
    self = [super init];
    if (self) {
        [self initializeObject];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initializeObject];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initializeObject];
    }
    return self;
}

- (void)initializeObject {
    self.backgroundColor = [UIColor clearColor];
    [self createImageView];
    [self addSubview:self.contentImageView];
    [self sendSubviewToBack:self.contentImageView];
}

#pragma mark - Public methods
- (CGSize)imageSizeFromViewSize:(CGSize)viewSize {
    return viewSize;
}

- (void)redrawImage {
    if (self.superview != nil && self.contentImageView != nil && self.drawBlock != nil) {
        [self forceRedrawImage];
    }
}

- (NSString*)imageIdentifier {
    NSMutableString* imageIdentifier = [NSMutableString stringWithCapacity:128];
    [imageIdentifier appendString:self.reuseIdentifier];
    if ([self redrawOnSizeChange]) {
        [imageIdentifier appendFormat:@"-%@", NSStringFromCGSize(self.frame.size)];
    }
    if ([self redrawOnStatusChange]) {
        [imageIdentifier appendFormat:@"-state(%@%@%@)",
         _highlighted ? @"h" : @"n",
         _enabled ? @"e" : @"n",
         _selected ? @"s" : @"n"];
    }
    return imageIdentifier;
}

- (CGSize)imageSize {
    return self.contentImageView.frame.size;
}

- (void)didMoveToSuperview {
    [super didMoveToSuperview];
    [self redrawImage];
}

#pragma mark - Property accessors
- (NSString *)reuseIdentifier {
    if (_reuseIdentifier == nil) {
        return NSStringFromClass([self class]);
        
    }
    return _reuseIdentifier;
}

- (void)setSelected:(BOOL)selected {
    _selected = selected;
    if ([self redrawOnStatusChange]) {
        [self redrawImage];
    }
}

- (void)setEnabled:(BOOL)enabled {
    _enabled = enabled;
    if ([self redrawOnStatusChange]) {
        [self redrawImage];
    }
}

- (void)setHighlighted:(BOOL)highlighted {
    _highlighted = highlighted;
    if ([self redrawOnStatusChange]) {
        [self redrawImage];
    }
}

- (void)setFrame:(CGRect)frame {
    CGSize previousSize = self.frame.size;
    [super setFrame:frame];
    if (!CGSizeEqualToSize(frame.size, previousSize)) {
        [self layoutImageView];
        if ([self redrawOnSizeChange]) {
            [self redrawImage];
        }
    }
}

#pragma mark - Animated status change
- (void)setEnabled:(BOOL)enabled animated:(BOOL)animated {
    if (animated) {
        [UIView animateWithDuration:kAnimationDuration animations:^{
            self.enabled = enabled;
        }];
    }
    else {
        self.enabled = enabled;
    }
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    if (animated) {
        CATransition* transition = [CATransition animation];
        transition.type = kCATransitionFade;
        transition.duration = kAnimationDuration;
        [self.layer addAnimation:transition forKey:@"highlightAnimation"];
    }
    self.highlighted = highlighted;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    if (animated) {
        CATransition* transition = [CATransition animation];
        transition.type = kCATransitionFade;
        transition.duration = kAnimationDuration;
        [self.layer addAnimation:transition forKey:@"selectedAnimation"];
    }
    self.selected = selected;
}


#pragma mark - Private methods
- (void)createImageView {
    self.contentImageView = [[UIImageView alloc] init];
    _contentImageView.contentMode = UIViewContentModeScaleToFill;
    _contentImageView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin |
        UIViewAutoresizingFlexibleWidth |
        UIViewAutoresizingFlexibleRightMargin |
        UIViewAutoresizingFlexibleTopMargin |
        UIViewAutoresizingFlexibleHeight |
        UIViewAutoresizingFlexibleBottomMargin;
    
    [self layoutImageView];
}

- (void)layoutImageView {
    CGSize size = [self imageSizeFromViewSize:self.frame.size];
    CGRect frame = CGRectMake((self.frame.size.width - size.width) / 2,
                              (self.frame.size.height - size.height) / 2,
                              size.width,
                              size.height);
    _contentImageView.frame = frame;
}

- (void)forceRedrawImage {
    UIImage* image = [UIImage imageWithIdentifier:[self imageIdentifier]
                                           opaque:self.opaque
                                          forSize:self.imageSize
                                  andDrawingBlock:^{
                                      self.drawBlock(self);
                                  }];
    
    self.contentImageView.image = image;
}

@end
