//
//  TCSOAPRequest.m
//  TwinCodersLibrary
//
//  Created by Alex Gutiérrez on 09/10/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "TCSOAPRequest.h"
#import "XMLReader.h"
#import "TCRequestParam.h"

// Error domain
static NSString* const kErrorDomain  = @"com.twincoders.TCSOAPRequest";

// Request Params
static NSString* const kRequestSOAPSchemaDefinition = @"xmlns:%@=\"%@\"";
static NSString* const kRequestSOAPSchemaFormat = @"%@:%@";
static NSString* const kRequestSOAPNoSchemaUse = @"%@  xmlns:n0=\"http://www.w3.org/2001/XMLSchema\"";
static NSString* const kRequestSOAPEnvelopment =
@"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" %@>"
"<soapenv:Header>"
"%@"
"</soapenv:Header>"
"<soapenv:Body>"
"<%@>"
"%@"
"</%@>"
"</soapenv:Body>"
"</soapenv:Envelope>";
static NSString* const kSoapActionHeader = @"SOAPAction";

// Response
static NSString* const kResponseSchemaSeparator =   @":";
static NSString* const kResponseValueKey =          @"text";
static NSString* const kResponseSoapEnvelopeKey =   @"Envelope";
static NSString* const kResponseSoapBodyKey =       @"Body";
static NSString* const kResponseSoapFaultKey =      @"Fault";
static NSString* const kResponseSoapFaultStringKey= @"faultstring";

@implementation TCSOAPRequest

#pragma mark - Request launch

-(NSString*) createBodyContent {
    NSString* bodyContent = [super createBodyContent];
    NSString* schemaDefinition;
    NSString* openContentWrapper;
    NSString* closeContentWrapper;
    // If request has schema
    if (![self ignoreSchema]) {
        schemaDefinition = [NSString stringWithFormat:kRequestSOAPSchemaDefinition, self.soapShemaName ,self.soapShemaURL];
        openContentWrapper = closeContentWrapper = [NSString stringWithFormat:kRequestSOAPSchemaFormat, self.soapShemaName, self.soapContentWrapper];
    } else {
        schemaDefinition = @"";
        openContentWrapper = [NSString stringWithFormat:kRequestSOAPNoSchemaUse, self.soapContentWrapper];
        closeContentWrapper = self.soapContentWrapper;
    }
    NSString* soapHeaderString = [self stringFromRequestParamsArray:[self soapHeaderParams]];
    NSString* soapContentString = [NSString stringWithFormat:kRequestSOAPEnvelopment, schemaDefinition, soapHeaderString, openContentWrapper, bodyContent, closeContentWrapper];
    return soapContentString;
}

-(NSArray*) soapHeaderParams {
    return [NSArray array];
}

-(ASIHTTPRequest *)createAsiRequest {
    ASIHTTPRequest* request = [super createAsiRequest];
    // Include soap action header
    [request addRequestHeader:kSoapActionHeader value:self.soapAction];
    return request;
}

#pragma mark - Request response

-(NSString*) stringByCleaningKey:(NSString*)key {
    NSString* returnKey = nil;
    NSArray* keyParts = [key componentsSeparatedByString:kResponseSchemaSeparator];
    if (keyParts.count > 1) {
        returnKey = [keyParts lastObject];
    } else {
        returnKey = key;
    }
    return returnKey;
}

-(NSDictionary *)onProcessResponseDictionary:(NSDictionary *)response withError:(NSError **)error {
    NSDictionary* dictionary = [super onProcessResponseDictionary:response withError:error];
    if (error != nil) {
        dictionary = [[dictionary objectForKey:kResponseSoapEnvelopeKey] objectForKey:kResponseSoapBodyKey];
        if (dictionary != nil) {
            // Check soap error
            NSDictionary* errorDictionary = [dictionary objectForKey:kResponseSoapFaultKey];
            if (errorDictionary.count > 0) {
                NSString* errorMessage = [errorDictionary objectForKey:kResponseSoapFaultStringKey];
                *error = [NSError errorWithDomain:kErrorDomain code:0 userInfo:[NSDictionary dictionaryWithObject:errorMessage forKey:NSLocalizedDescriptionKey]];
            }
        } else {
            NSString* errorMessage = NSLocalizedString(@"EMPTY_SOAP_RESPONSE", nil);
            *error = [NSError errorWithDomain:kErrorDomain code:0 userInfo:[NSDictionary dictionaryWithObject:errorMessage forKey:NSLocalizedDescriptionKey]];
        }
        
    }
    return dictionary;
}

-(NSString*) url {
    return [NSString stringWithFormat:self.soapUrlPattern, self.soapEndpoint];
}

@end