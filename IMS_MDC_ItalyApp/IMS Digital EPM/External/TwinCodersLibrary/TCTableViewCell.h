//
//  TCTableViewCell.h
//  TwinCodersLibrary
//
//  Created by Guillermo Gutiérrez on 01/02/13.
//  Copyright (c) 2013 TwinCoders S.L. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TCTableViewCell : UITableViewCell

@property (nonatomic) BOOL highlightOnSelected;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *highlightElements;

- (void)refreshStatusAnimated:(BOOL)animated;

@end
