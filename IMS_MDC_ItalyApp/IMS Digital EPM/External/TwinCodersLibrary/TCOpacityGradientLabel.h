//
//  TCOpacityGradientLabel.h
//  TwinCodersLibrary
//
//  Created by Guillermo Gutiérrez on 04/06/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TCOpacityGradientLabel : UILabel
@property (nonatomic, strong) NSNumber* gradientStartPoint; // Float from 0.0 to 1.0
@end
