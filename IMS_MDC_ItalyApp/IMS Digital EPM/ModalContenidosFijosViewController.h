//
//  AgadeceColaboracionViewController.h
//  IMS Digital EPM
//
//  Created by Ricardo Berzal on 24/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ModalContenidosFijosViewControllerProtocol
@required
- (void)CierreModal;
@end

@interface ModalContenidosFijosViewController : UIViewController {
    
    IBOutlet UILabel* _labelFirst1;
    IBOutlet UILabel* _labelFirst2;
    IBOutlet UILabel* _label;
    IBOutlet UIImageView* _imgIcon;
    
    bool _firstTime;
    
    NSObject<ModalContenidosFijosViewControllerProtocol>* _delegate;  //objeto delegado
}

- (ModalContenidosFijosViewController*)initWithDelegate: (NSObject<ModalContenidosFijosViewControllerProtocol>*) delegate fistTime:(bool)firstTime;

@end
