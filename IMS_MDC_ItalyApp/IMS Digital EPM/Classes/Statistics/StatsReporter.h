//
//  StatsReporter.h
//  IMS Digital EPM
//
//  Created by Diego Prados on 18/12/12.
//
//

#import <Foundation/Foundation.h>
#import "TwinCodersLibrary.h"

typedef enum {
    kStatsEventTypeCreatePatient,
    kStatsEventTypeUpdatePatient,
    kStatsEventTypeEnterHelp,
    kStatsEventTypeStartSession,
    kStatsEventTypeLogin
} StatsEventType;

@interface StatsReporter : NSObject

@property (nonatomic, copy) NSString* userId;

/** @brief Singleton access */
+ (StatsReporter*)sharedStatsReporter;

- (void)startEvent:(StatsEventType)eventType;
- (void)endEvent:(StatsEventType)eventType;
- (void)endEvent:(StatsEventType)eventType withPatientId:(NSInteger)patientId;
- (void)startCountingEvent:(StatsEventType)eventType;
- (void)endCountingEvent:(StatsEventType)eventType;
- (void)sendStats;

@end
