//
//  ChangeNotifier.m
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 29/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "ChangeNotifier.h"
#import "SyncAction.h"
#import "SyncActionQueue.h"
#import "EntityFactory.h"

@interface ChangeNotifier()
@property (nonatomic, readonly) NSOperationQueue* operationQueue;
@property (nonatomic, assign) BOOL notificationsPaused;
@end

@implementation ChangeNotifier
@synthesize operationQueue = _operationQueue;
@synthesize notificationsPaused = _notificationsPaused;


#pragma mark - Singleton access
static ChangeNotifier* sharedChangeNotifierInstance = nil;
+ (ChangeNotifier*)sharedNotifier {
    if (sharedChangeNotifierInstance == nil) {
        sharedChangeNotifierInstance = [[ChangeNotifier alloc] init];
    }
    return sharedChangeNotifierInstance;
}

#pragma mark - Init and dealloc
- (id)init {
    if (( self = [super init] )) {
        _operationQueue = [[NSOperationQueue alloc] init];
        _operationQueue.maxConcurrentOperationCount = 1;
    }
    return self;
}

- (void)dealloc {
    [_operationQueue cancelAllOperations];
    [_operationQueue release];
    [super dealloc];
}

#pragma mark - Private methods
- (BOOL)alreadyNotifying {
    return _operationQueue.operationCount > 0;
}

- (void)processNextAction {
    if (!_notificationsPaused) {
        // Ensure that we're running in main thread to access persistent store
        if (![NSThread isMainThread]) {
            [self performSelectorOnMainThread:@selector(processNextAction) withObject:nil waitUntilDone:NO];
        }
        
        // Fetch the first confirmed sync action
        SyncAction* action = [[SyncActionQueue sharedQueue] firstConfirmedSyncAction];
        if (action != nil) {
            DAssert(action.actionType != nil, @"Action type is nil");
            DAssert(action.targetObject != nil, @"Action target is nil");
            
            ChangeNotificationOperationType changeType = kChangeNotificationOperationUpdate;
            if ([action.actionType isEqualToString:kSyncActionUpdate]) {
                changeType = kChangeNotificationOperationUpdate;
            }
            else if ([action.actionType isEqualToString:kSyncActionAdd]) {
                changeType = kChangeNotificationOperationAdd;
            }
            else if ([action.actionType isEqualToString:kSyncActionDelete]) {
                changeType = kChangeNotificationOperationDelete;
            }
            else {
                DLog(@"Unknown action type %@", action.actionType);
            }
            
            // Create the operation
            ChangeNotificationOperation* operation = [[ChangeNotificationOperation alloc] initWithObjectID:action.targetObject.objectID operationType:changeType syncActionID:action.objectID delegate:self];
            
            // Add the operation to the queue
            [_operationQueue addOperation:operation];
            
            [operation release];
            
        }
        else {
            DLog(@"No more actions to sync");
            //Deepak_Carpenter : Added for Checkdataversion
           // [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"pagenumber"];
        }
    }
}

#pragma mark - Public methods
- (void)startNotifying {
    if (!self.notificationsPaused && ![self alreadyNotifying]) {
        [self processNextAction];
    }
}

- (void)pauseNotifying {
    _notificationsPaused = YES;
}

- (void)unpauseNotifying {
    _notificationsPaused = NO;
    [self startNotifying];
}

- (void)stopNotifying {
    [_operationQueue cancelAllOperations];
}

#pragma mark - ChangeNotificationOperationDelegate
- (void)changeNotificationOperation:(ChangeNotificationOperation*)operation didFailWithError:(NSError*)error {
    // If connection error, try again later.
    DLog(@"Received error executing operation %@: %@, %@", operation, error, [error userInfo]);
}

- (void)changeNotificationOperationDidComplete:(ChangeNotificationOperation*)operation {
    NSManagedObjectContext* managedObjectContext = [[EntityFactory sharedEntityFactory] createContext];
    
    // Delete the SyncAction so it's not performed twice
    NSManagedObject* action = [managedObjectContext objectWithID:operation.syncActionID];
    [managedObjectContext deleteObject:action];
    
    // If it was a delete action, delete the target object
    if (operation.operationType == kChangeNotificationOperationDelete) {
        NSManagedObject* targetObject = [managedObjectContext objectWithID:operation.syncObjectID];
        [managedObjectContext deleteObject:targetObject];
    }
    
    NSError* error = nil;
    [managedObjectContext save:&error];
    if (error != nil) {
        DLog(@"Error deleting SyncAction: %@, %@", error, [error userInfo]);
    }
    
    [self processNextAction];
}

@end
