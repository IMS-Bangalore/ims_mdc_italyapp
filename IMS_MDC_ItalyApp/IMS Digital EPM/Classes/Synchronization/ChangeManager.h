//
//  ChangeManager.h
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 27/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DoctorInfo.h"
#import "Patient.h"
#import "Diagnosis.h"
#import "Treatment.h"
#import "SessionExpiredHandler.h"

@interface ChangeManager : NSObject

/** @brief Access the singleton instance */
+ (ChangeManager*)manager;

#pragma mark Confirm and discard changes
/** @brief Confirm to perform all the operations */
- (void)confirmChangesToEntity:(Synchronizable*)entity;
- (void)confirmChangesToEntity:(Synchronizable*)entity recursive:(BOOL)recursive;

/** @brief Discard all the unconfirmed pending changes */
- (void)discardChanges;
- (void)recursiveCancelActions:(Synchronizable*)entity;

#pragma mark - Generic methods
- (void)updateEntity:(Synchronizable*)entity;

#pragma mark DoctorInfo
- (DoctorInfo*)createDoctorInfo;
- (void)updateDoctorInfo:(DoctorInfo*)doctorInfo;
- (void)deleteDoctorInfo:(DoctorInfo*)doctorInfo;

#pragma mark Patient
- (Patient*)addPatientToDoctorInfo:(DoctorInfo*)doctorInfo;
- (void)updatePatient:(Patient*)patient;
- (void)deletePatient:(Patient*)patient;

#pragma mark Diagnosis
- (Diagnosis*)addDiagnosisToPatient:(Patient*)patient;
- (void)updateDiagnosis:(Diagnosis*)diagnosis;
- (void)deleteDiagnosis:(Diagnosis*)diagnosis;

#pragma mark Treatment
- (Treatment*)addTreatmentToDiagnosis:(Diagnosis*)diagnosis;
- (void)updateTreatment:(Treatment*)treatment;
- (void)deleteTreatment:(Treatment*)treatment;

@end
