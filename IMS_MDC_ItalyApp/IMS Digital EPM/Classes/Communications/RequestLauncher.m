//
//  RequestLauncher.m
//  IMS Digital EPM
//
//  Created by Alex Guti on 26/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "RequestLauncher.h"
#import "RequestParam.h"
#import "ASIFormDataRequest.h"

// Private methods
@interface RequestLauncher() 
@end

@implementation RequestLauncher

static NSString* const kServerUrlKey =          @"serverURL";
// Request Params
static NSString* const kCountryCodeKey =        @"country";
static NSString* const kRequestParamFormat =    @"\n<%@>%@</%@>";
static NSString* const kRequestParamSeparator = @"";

// XML Format contants
static NSString* const kRequestXMLEnvelope =    @"<actionRequest id=\"%@\">%@\n</actionRequest>";

- (id)init {
    self = [super init];
    if (self) {
        // Init active requests array
        _activeRequests = [[NSMutableDictionary alloc] init];
        // Init Communications properties helper
        _communicationStrings = [[CommunicationStrings alloc] init];
    }
    return self;
}

- (void)dealloc {
    [_activeRequests release];
    [_communicationStrings release];
    [super dealloc];
}

-(NSString*) requestContentStringFromRequestParamsArray:(NSMutableArray*)paramsArray {
    NSString* contentString = @"";
    for (RequestParam* param in paramsArray) {
        if (contentString.length > 0) {
            contentString = [contentString stringByAppendingFormat:kRequestParamSeparator];
        }
        contentString = [contentString stringByAppendingFormat:kRequestParamFormat, param.key, param.value, param.key];
       // NSLog(@"Request Key %@ -- Value %@ -- Array %@",param.key,param.value,paramsArray);
    }
    return contentString;
}

-(ASIFormDataRequest*)createAsiRequestFromRequest:(BaseRequest*)baseRequest {
 

   NSLog(@"RL Request Name %@",baseRequest.name);
    NSString *urlString = kPRODBaseURL;
//  NSString *urlString = KDEVBaseURL;
        
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]stringForKey:@"SessionToken"];


   if ([baseRequest.name isEqualToString:@"insertDoctor"]) {
        
        
        NSString *doctorURLString = [urlString stringByAppendingFormat:@"%@/%@?responseFormat=%@",kDoctorInfo,savedValue,@"XML"];
        NSURL* url = [NSURL URLWithString:doctorURLString];
        
        // Instance ASI Request with url
        ASIFormDataRequest* asiRequest = [[ASIFormDataRequest alloc] initWithURL: url];
        [asiRequest setStringEncoding:NSUTF8StringEncoding];
        // Avoid redirect
        asiRequest.shouldRedirect = NO;

        // Create content params
        NSMutableArray* paramsArray = [[NSMutableArray alloc] init];
        [paramsArray addObjectsFromArray:baseRequest.contentParams];
        // If parameter are not custom, include them in a formatted XML
        NSString* requestParamString = [self requestContentStringFromRequestParamsArray:paramsArray];
        NSLog(@"requestParamString %@", requestParamString);
        [paramsArray release];
        NSString* formattedContent = [NSString stringWithFormat:kRequestXMLEnvelope, baseRequest.name, requestParamString];
        DLog(@"Formatted content: %@", formattedContent);
        NSLog(@"Formatted content: %@", formattedContent);
        [asiRequest appendPostData:[formattedContent dataUsingEncoding:NSUTF8StringEncoding]];
        [asiRequest buildPostBody];
        return [asiRequest autorelease];

        
    }
    
    else if ([baseRequest.name isEqualToString:@"insertPatient"]) {
        
        
        
        NSString *treatmentURLString = [urlString stringByAppendingFormat:@"%@/%@?responseFormat=%@",kPatientInfo,savedValue,@"XML"];
        NSURL* url = [NSURL URLWithString:treatmentURLString];
        
        // Instance ASI Request with url
        ASIFormDataRequest* asiRequest = [[ASIFormDataRequest alloc] initWithURL: url];
        [asiRequest setStringEncoding:NSUTF8StringEncoding];
        // Avoid redirect
        asiRequest.shouldRedirect = NO;
        
        // Create content params
        NSMutableArray* paramsArray = [[NSMutableArray alloc] init];
        [paramsArray addObjectsFromArray:baseRequest.contentParams];
        // If parameter are not custom, include them in a formatted XML
        NSString* requestParamString = [self requestContentStringFromRequestParamsArray:paramsArray];
        NSLog(@"requestParamString %@", requestParamString);
        [paramsArray release];
        NSString* formattedContent = [NSString stringWithFormat:kRequestXMLEnvelope, baseRequest.name, requestParamString];
        DLog(@"Formatted content: %@", formattedContent);
        NSLog(@"Formatted content: %@", formattedContent);
        [asiRequest appendPostData:[formattedContent dataUsingEncoding:NSUTF8StringEncoding]];
        [asiRequest buildPostBody];
        return [asiRequest autorelease];
    }

    else if ([baseRequest.name isEqualToString:@"insertMedicine"]||[baseRequest.name isEqualToString:@"updateMedicine"]||[baseRequest.name isEqualToString:@"removeMedicine"]) {
        
        NSString *treatmentURLString = [urlString stringByAppendingFormat:@"%@/%@?responseFormat=%@",kTreatmentInfo,savedValue,@"XML"];
        NSURL* url = [NSURL URLWithString:treatmentURLString];
        
        // Instance ASI Request with url
        ASIFormDataRequest* asiRequest = [[ASIFormDataRequest alloc] initWithURL: url];
        [asiRequest setStringEncoding:NSUTF8StringEncoding];
        // Avoid redirect
        asiRequest.shouldRedirect = NO;
        
        // Create content params
        NSMutableArray* paramsArray = [[NSMutableArray alloc] init];
        [paramsArray addObjectsFromArray:baseRequest.contentParams];
        // If parameter are not custom, include them in a formatted XML
        NSString* requestParamString = [self requestContentStringFromRequestParamsArray:paramsArray];
        NSLog(@"requestParamString %@", requestParamString);
        [paramsArray release];
        NSString* formattedContent = [NSString stringWithFormat:kRequestXMLEnvelope, baseRequest.name, requestParamString];
        DLog(@"Formatted content: %@", formattedContent);
        NSLog(@"Formatted content: %@", formattedContent);
        [asiRequest appendPostData:[formattedContent dataUsingEncoding:NSUTF8StringEncoding]];
        [asiRequest buildPostBody];
        return [asiRequest autorelease];
        
    }
    
    
    
    else if ([baseRequest.name isEqualToString:@"updatePatient"]||[baseRequest.name isEqualToString:@"removePatient"]) {
        
        NSString *treatmentURLString = [urlString stringByAppendingFormat:@"%@/%@?responseFormat=%@",kPatientInfo,savedValue,@"XML"];
        NSURL* url = [NSURL URLWithString:treatmentURLString];
        
        // Instance ASI Request with url
        ASIFormDataRequest* asiRequest = [[ASIFormDataRequest alloc] initWithURL: url];
        [asiRequest setStringEncoding:NSUTF8StringEncoding];
        // Avoid redirect
        asiRequest.shouldRedirect = NO;
        
        // Create content params
        NSMutableArray* paramsArray = [[NSMutableArray alloc] init];
        [paramsArray addObjectsFromArray:baseRequest.contentParams];
        // If parameter are not custom, include them in a formatted XML
        NSString* requestParamString = [self requestContentStringFromRequestParamsArray:paramsArray];
        NSLog(@"requestParamString %@", requestParamString);
        [paramsArray release];
        NSString* formattedContent = [NSString stringWithFormat:kRequestXMLEnvelope, baseRequest.name, requestParamString];
        DLog(@"Formatted content: %@", formattedContent);
        NSLog(@"Formatted content: %@", formattedContent);
        [asiRequest appendPostData:[formattedContent dataUsingEncoding:NSUTF8StringEncoding]];
        [asiRequest buildPostBody];
        return [asiRequest autorelease];
    }
    
    
    else if ([baseRequest.name isEqualToString:@"insertDiagnosis"]||[baseRequest.name isEqualToString:@"updateDiagnosis"]||[baseRequest.name isEqualToString:@"removeDiagnosis"]) {
        
        NSString *treatmentURLString = [urlString stringByAppendingFormat:@"%@/%@?responseFormat=%@",kDiagnosisInfo,savedValue,@"XML"];
        NSURL* url = [NSURL URLWithString:treatmentURLString];
        
        // Instance ASI Request with url
        ASIFormDataRequest* asiRequest = [[ASIFormDataRequest alloc] initWithURL: url];
        [asiRequest setStringEncoding:NSUTF8StringEncoding];
        // Avoid redirect
        asiRequest.shouldRedirect = NO;
        
        // Create content params
        NSMutableArray* paramsArray = [[NSMutableArray alloc] init];
        [paramsArray addObjectsFromArray:baseRequest.contentParams];
        // If parameter are not custom, include them in a formatted XML
        NSString* requestParamString = [self requestContentStringFromRequestParamsArray:paramsArray];
        NSLog(@"requestParamString %@", requestParamString);
        [paramsArray release];
        NSString* formattedContent = [NSString stringWithFormat:kRequestXMLEnvelope, baseRequest.name, requestParamString];
        DLog(@"Formatted content: %@", formattedContent);
        NSLog(@"Formatted content: %@", formattedContent);
        [asiRequest appendPostData:[formattedContent dataUsingEncoding:NSUTF8StringEncoding]];
        [asiRequest buildPostBody];
        return [asiRequest autorelease];
        
    }
    //Send Question
    else if ([baseRequest.name isEqualToString:@"sendQuestion"]) {
        
        
        NSString *sendQuestionURLString = [urlString stringByAppendingFormat:@"SendQuestion/%@?ResponseFormat=%@",savedValue,@"XML"];
        NSURL* url = [NSURL URLWithString:sendQuestionURLString];
        // Instance ASI Request with url
        ASIFormDataRequest* asiRequest = [[ASIFormDataRequest alloc] initWithURL: url];
        [asiRequest setStringEncoding:NSUTF8StringEncoding];
        // Avoid redirect
        asiRequest.shouldRedirect = NO;
        
        // Create content params
        NSMutableArray* paramsArray = [[NSMutableArray alloc] init];
        [paramsArray addObjectsFromArray:baseRequest.contentParams];
        // If parameter are not custom, include them in a formatted XML
        NSString* requestParamString = [self requestContentStringFromRequestParamsArray:paramsArray];
        NSLog(@"requestParamString %@", requestParamString);
        [paramsArray release];
        NSString* formattedContent = [NSString stringWithFormat:kRequestXMLEnvelope, baseRequest.name, requestParamString];
        DLog(@"Formatted content: %@", formattedContent);
        NSLog(@"Formatted content: %@", formattedContent);
        [asiRequest appendPostData:[formattedContent dataUsingEncoding:NSUTF8StringEncoding]];
        [asiRequest buildPostBody];
        return [asiRequest autorelease];
    }
    //Get Answer
    else if ([baseRequest.name isEqualToString:@"getAnswers"]) {
        
        
        NSString *GetAnswersURLString = [urlString stringByAppendingFormat:@"GetAnswers/%@?ResponseFormat=%@",savedValue,@"XML"];        NSURL* url = [NSURL URLWithString:GetAnswersURLString];
        // Instance ASI Request with url
        ASIFormDataRequest* asiRequest = [[ASIFormDataRequest alloc] initWithURL: url];
        [asiRequest setStringEncoding:NSUTF8StringEncoding];
        // Avoid redirect
        asiRequest.shouldRedirect = NO;
        
        // Create content params
        NSMutableArray* paramsArray = [[NSMutableArray alloc] init];
        [paramsArray addObjectsFromArray:baseRequest.contentParams];
        // If parameter are not custom, include them in a formatted XML
        NSString* requestParamString = [self requestContentStringFromRequestParamsArray:paramsArray];
        NSLog(@"requestParamString %@", requestParamString);
        [paramsArray release];
        NSString* formattedContent = [NSString stringWithFormat:kRequestXMLEnvelope, baseRequest.name, requestParamString];
        DLog(@"Formatted content: %@", formattedContent);
        NSLog(@"Formatted content: %@", formattedContent);
        [asiRequest appendPostData:[formattedContent dataUsingEncoding:NSUTF8StringEncoding]];
        [asiRequest buildPostBody];
        return [asiRequest autorelease];
    }

    
    //Deepak Carpenter Commented Spain Production URL Call
    else {
//    // Server URL
 //   NSString* serverUrl = [_communicationStrings stringWithKey:kServerUrlKey];
    NSURL* url = [NSURL URLWithString:urlString];
//    // Instance ASI Request with url
    ASIFormDataRequest* asiRequest = [[ASIFormDataRequest alloc] initWithURL: url];
    [asiRequest setStringEncoding:NSUTF8StringEncoding];
//    // Avoid redirect
    asiRequest.shouldRedirect = NO;
    // Include auth headers
 //   [asiRequest addRequestHeader:[_communicationStrings stringWithKey:@"requestAuthUserNameKey"]
//                           value:[_communicationStrings stringWithKey:@"requestAuthUserNameValue"]];
//    [asiRequest addRequestHeader:[_communicationStrings stringWithKey:@"requestAuthPasswordKey"]
//                           value:[_communicationStrings stringWithKey:@"requestAuthPasswordValue"]];
   // Create content params
    NSMutableArray* paramsArray = [[NSMutableArray alloc] init];
    [paramsArray addObjectsFromArray:baseRequest.contentParams];
    // If parameter are not custom, include them in a formatted XML
    NSString* requestParamString = [self requestContentStringFromRequestParamsArray:paramsArray];
    [paramsArray release];
    NSString* formattedContent = [NSString stringWithFormat:kRequestXMLEnvelope, baseRequest.name, requestParamString];
    DLog(@"Formatted content: %@", formattedContent);
    [asiRequest appendPostData:[formattedContent dataUsingEncoding:NSUTF8StringEncoding]];
    [asiRequest buildPostBody];
    return [asiRequest autorelease];
    }
    return nil;
    
    ///////End
    
    //Ravi_Bukka: Commented to support new services
    // Server URL
    /*    NSString* serverUrl = [_communicationStrings stringWithKey:kServerUrlKey];
     NSURL* url = [NSURL URLWithString:serverUrl];
     // Instance ASI Request with url
     ASIFormDataRequest* asiRequest = [[ASIFormDataRequest alloc] initWithURL: url];
     [asiRequest setStringEncoding:NSUTF8StringEncoding];
     // Avoid redirect
     asiRequest.shouldRedirect = NO;
     // Include auth headers
     [asiRequest addRequestHeader:[_communicationStrings stringWithKey:@"requestAuthUserNameKey"]
     value:[_communicationStrings stringWithKey:@"requestAuthUserNameValue"]];
     [asiRequest addRequestHeader:[_communicationStrings stringWithKey:@"requestAuthPasswordKey"]
     value:[_communicationStrings stringWithKey:@"requestAuthPasswordValue"]];
     // Create content params
     NSMutableArray* paramsArray = [[NSMutableArray alloc] init];
     [paramsArray addObjectsFromArray:baseRequest.contentParams];
     // If parameter are not custom, include them in a formatted XML
     NSString* requestParamString = [self requestContentStringFromRequestParamsArray:paramsArray];
     [paramsArray release];
     NSString* formattedContent = [NSString stringWithFormat:kRequestXMLEnvelope, baseRequest.name, requestParamString];
     DLog(@"Formatted content: %@", formattedContent);
     [asiRequest appendPostData:[formattedContent dataUsingEncoding:NSUTF8StringEncoding]];
     [asiRequest buildPostBody];
     return [asiRequest autorelease];
     }
     return nil;
     */
   

}

//Ravi_Bukka: Added for CheckDataVersion Implementation

-(NSArray*) requestContentArryaFromRequestParamsArray:(NSMutableArray*)paramsArray {
    NSMutableArray* contentString = [[NSMutableArray alloc]init];
    for (RequestParam* param in paramsArray) {
        [contentString addObject:param.value];
        
    }
    return contentString;
}


//Ravi_Bukka: Added for CheckDataVersion Implementation

/* -(ASIHTTPRequest*)createUpdateAsiRequestFromRequest:(BaseRequest*)baseRequest {
    
    [baseRequest addRequestEndDelegate:self];
    NSLog(@"Update Request Name %@",baseRequest.name);
    NSString *urlString = kSTGBaseURL;
    //  NSString *urlString = KDEVBaseURL;
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]stringForKey:@"SessionToken"];
    
    //Ravi_Bukka: Added for CheckDataVersion Implementation
    
    
    
    NSLog(@"Coming in CheckDataVersion");
    
    NSMutableArray* paramsArray = [[NSMutableArray alloc] init];
    [paramsArray addObjectsFromArray:baseRequest.contentParams];
    NSArray* requestParamString = [self requestContentArryaFromRequestParamsArray:paramsArray];
    
    
    NSString *lastupdatedDate = [requestParamString objectAtIndex:2];
    NSString *initialPage = [requestParamString objectAtIndex:1];
    lastupdatedDate = [lastupdatedDate stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    NSString *checkDataVersionURLString = [urlString stringByAppendingFormat:@"%@SessionToken=%@&CountryCode=%@&LastUpdatedDate=%@&Page=%@&ResponseFormat=%@",kCheckDataVersion,savedValue,@"GR",lastupdatedDate,initialPage,@"XML"];
    NSLog(@"URL String --%@", checkDataVersionURLString);
    NSURL* url = [NSURL URLWithString:checkDataVersionURLString];
    
    NSLog(@"Webservice: %@", [url absoluteString]);
    
    ASIHTTPRequest *updateRequest = [ASIHTTPRequest requestWithURL:url];
    [updateRequest setDelegate:baseRequest];
    [updateRequest addRequestHeader:@"Accept" value:@"application/json"];
    
    updateRequest.shouldRedirect = NO;
    
    //[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    // Add request to active request array
    //[_activeRequests setObject:updateRequest forKey:baseRequest.requestId];
    
    // Just for testing
    //[updateRequest startAsynchronous];
    //    NSError *error = [updateRequest error];
    //    if (!error) {
    //        NSString *response = [updateRequest responseString];
    //        NSLog(@"Response: %@",response);
    //    }
    
    //    NSString *resp = [updateRequest responseString];
    //    NSLog(@"Response: %@",resp);
    return updateRequest;
} */


/** @brief Launches the selected request */
-(void) launchRequest:(BaseRequest*)request {
    NSLog(@"Request Name %@",request.name);

////CheckDataVersion
//    if ([request.name isEqualToString:@"checkDataVersion"]){
//        
//       
//        [request addRequestEndDelegate:self];
//        NSLog(@"Coming in checkdataversion");
//        
//        ASIHTTPRequest* asiRequest = [self createAsiRequestFromRequest:request];
//        asiRequest.delegate = request;
//        
//        DLog(@"Launching request: %@", asiRequest.url);
//        DLog(@"Request body:\n%@", [[[NSString alloc] initWithData:asiRequest.postBody encoding:NSUTF8StringEncoding] autorelease]);
//        
//        [asiRequest setRequestMethod:@"GET"];
//        
//        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
//        
//        // Add request to active request array
//        [_activeRequests setObject:asiRequest forKey:request.requestId];
//        
//        [asiRequest startAsynchronous];
//
//    }
    
   //Inserting
    if ([request.name isEqualToString:@"insertDoctor"]|| [request.name isEqualToString:@"insertPatient"] || [request.name isEqualToString:@"insertMedicine"]||[request.name isEqualToString:@"insertDiagnosis"]) {

        [request addRequestEndDelegate:self];
        NSLog(@"Coming in insertr");
        
        ASIHTTPRequest* asiRequest = [self createAsiRequestFromRequest:request];
        asiRequest.delegate = request;
        
        DLog(@"Launching request: %@", asiRequest.url);
        DLog(@"Request body:\n%@", [[[NSString alloc] initWithData:asiRequest.postBody encoding:NSUTF8StringEncoding] autorelease]);

//        [asiRequest addRequestHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
        [asiRequest setRequestMethod:@"POST"];

        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        
        // Add request to active request array
        [_activeRequests setObject:asiRequest forKey:request.requestId];
        
        [asiRequest startAsynchronous];
        
//        NSLog(@"request name %@", request.name);
//        
//        if ([_activeRequests objectForKey:request.requestId] == nil) {
//            [request addRequestEndDelegate:self];
//            ASIHTTPRequest* asiRequest = [self createAsiRequestFromRequest:request];
//            asiRequest.delegate = request;
//            DLog(@"Launching request: %@", asiRequest.url);
//            DLog(@"Request body:\n%@", [[[NSString alloc] initWithData:asiRequest.postBody encoding:NSUTF8StringEncoding] autorelease]);
//            // Display network activity indicator
//            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
//            // Add request to active request array
//            [_activeRequests setObject:asiRequest forKey:request.requestId];
//            // Start request
//            [asiRequest startAsynchronous];
//        } else {
//            DLog(@"Equal request already launched. Ignoring...");
//        }
        
    }
    
       
           
  
//Updating
    //Ravi_Bukka: Added request name "updateDiagnosis" to handle update diagnosis
      else if ([request.name isEqualToString:@"updatePatient"]||[request.name isEqualToString:@"updateMedicine"]||[request.name isEqualToString:@"updateDiagnosis"]) {
        
        NSLog(@"Coming in %@",request.name);
        
          [request addRequestEndDelegate:self];
          NSLog(@"Coming in insertr");
          
          ASIHTTPRequest* asiRequest = [self createAsiRequestFromRequest:request];
          asiRequest.delegate = request;
          
          DLog(@"Launching request: %@", asiRequest.url);
          DLog(@"Request body:\n%@", [[[NSString alloc] initWithData:asiRequest.postBody encoding:NSUTF8StringEncoding] autorelease]);
          
//    [asiRequest addRequestHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
          [asiRequest setRequestMethod:@"PUT"];
          
          [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
          
          // Add request to active request array
          [_activeRequests setObject:asiRequest forKey:request.requestId];
          
          [asiRequest startAsynchronous];
        
    }

    
    
//Deleting
    
      else if ([request.name isEqualToString:@"removePatient"]||[request.name isEqualToString:@"removeDiagnosis"]||[request.name isEqualToString:@"removeMedicine"]) {
        
        NSLog(@"Coming in Remove");
        
          [request addRequestEndDelegate:self];
          NSLog(@"Coming in insertr");
          
          ASIHTTPRequest* asiRequest = [self createAsiRequestFromRequest:request];
          asiRequest.delegate = request;
          
          DLog(@"Launching request: %@", asiRequest.url);
          DLog(@"Request body:\n%@", [[[NSString alloc] initWithData:asiRequest.postBody encoding:NSUTF8StringEncoding] autorelease]);
          
//          [asiRequest addRequestHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
          [asiRequest setRequestMethod:@"DELETE"];
          
          [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
          
          // Add request to active request array
          [_activeRequests setObject:asiRequest forKey:request.requestId];
          
          [asiRequest startAsynchronous];
        
    }
    
    //sendQuestion
      else if ([request.name isEqualToString:@"sendQuestion"]) {
          
          NSLog(@"Coming in sendQuestion");
          
          [request addRequestEndDelegate:self];
          
          ASIHTTPRequest* asiRequest = [self createAsiRequestFromRequest:request];
          asiRequest.delegate = request;
          
          DLog(@"Launching request: %@", asiRequest.url);
          DLog(@"Request body:\n%@", [[[NSString alloc] initWithData:asiRequest.postBody encoding:NSUTF8StringEncoding] autorelease]);
          
          [asiRequest setRequestMethod:@"POST"];
          
          [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
          
          // Add request to active request array
          [_activeRequests setObject:asiRequest forKey:request.requestId];
          
          [asiRequest startAsynchronous];
          
      }
      else if ([request.name isEqualToString:@"getAnswers"]) {
          
          NSLog(@"Coming in getAnswers");
          
          [request addRequestEndDelegate:self];
          
          ASIHTTPRequest* asiRequest = [self createAsiRequestFromRequest:request];
          asiRequest.delegate = request;
          
          DLog(@"Launching request: %@", asiRequest.url);
          DLog(@"Request body:\n%@", [[[NSString alloc] initWithData:asiRequest.postBody encoding:NSUTF8StringEncoding] autorelease]);
          
          [asiRequest setRequestMethod:@"POST"];
          
          [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
          
          // Add request to active request array
          [_activeRequests setObject:asiRequest forKey:request.requestId];
          
          [asiRequest startAsynchronous];
          
      }
    
      else if ([request.name isEqualToString:@"CheckDataVersion"]) {
          
          NSLog(@"CheckDataVersion");
          //        [self createAsiRequestFromRequest:request];
          [request addRequestEndDelegate:self];
          
          //ASIHTTPRequest* asiRequest = [self createUpdateAsiRequestFromRequest:request];
          
          NSLog(@"Update Request Name %@",request.name);
          NSString *urlString = kPRODBaseURL;
          //  NSString *urlString = KDEVBaseURL;
          
          NSString *savedValue = [[NSUserDefaults standardUserDefaults]stringForKey:@"SessionToken"];
          
          //Ravi_Bukka: Added for CheckDataVersion Implementation
          
          
          
          NSLog(@"Coming in CheckDataVersion");
          
          NSMutableArray* paramsArray = [[NSMutableArray alloc] init];
          [paramsArray addObjectsFromArray:request.contentParams];
          NSArray* requestParamString = [self requestContentArryaFromRequestParamsArray:paramsArray];
          
    
          NSString *lastupdatedDate = [requestParamString objectAtIndex:2];
          NSString *initialPage = [requestParamString objectAtIndex:1];
         [[NSUserDefaults standardUserDefaults] setObject:initialPage forKey:@"pagenumber"];
          
          //Deepak_Carpenter : Added for Page Number updation
                    
//          if (![initialPage isEqualToString:@"0"]) {
//
//              [[NSUserDefaults standardUserDefaults] setObject:initialPage forKey:@"pagenumber"];
//              [[NSUserDefaults standardUserDefaults] synchronize];
//          }
//          else   [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"pagenumber"];
//          
//        if (![[[NSUserDefaults standardUserDefaults]
//                 stringForKey:@"pagenumber"] isEqualToString:@"0"]) {
//        initialPage=[[NSUserDefaults standardUserDefaults]
//                           stringForKey:@"pagenumber"];
//          }else
//              initialPage = [requestParamString objectAtIndex:1];
//          
          
          

          
          lastupdatedDate = [lastupdatedDate stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
          
          
        //  lastupdatedDate=@"2014/09/14%2005:30:00";
          NSLog(@"last updated date %@", lastupdatedDate);
          
          
          NSString *checkDataVersionURLString = [urlString stringByAppendingFormat:@"%@SessionToken=%@&CountryCode=%@&LastUpdatedDate=%@&Page=%@&ResponseFormat=%@",kCheckDataVersion,savedValue,@"IT",lastupdatedDate,initialPage,@"XML"];
          NSLog(@"URL String --%@", checkDataVersionURLString);
          NSURL* url = [NSURL URLWithString:checkDataVersionURLString];
          
          NSLog(@"Webservice: %@", [url absoluteString]);
          
          ASIHTTPRequest *updateRequest = [ASIHTTPRequest requestWithURL:url];
          [updateRequest setDelegate:request];
          [updateRequest addRequestHeader:@"Accept" value:@"application/json"];
          
          updateRequest.shouldRedirect = NO;
          

          
          // Just for testing
          //[updateRequest startAsynchronous];

          
          
        [updateRequest setRequestMethod:@"GET"];
          
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
          
        // Add request to active request array
        [_activeRequests setObject:updateRequest forKey:request.requestId];
        //
        [updateRequest startAsynchronous];
          //
          //        
          NSString *resp = [updateRequest responseString];
          NSLog(@"Response: %@",resp);
      }
    

    
    

    // Deepak Carpenter Commented //Calling Spain production Services
//    
//    else {
//        
//        NSLog(@"Comeing in Else");
//    [request addParam:[_communicationStrings stringWithKey:@"countryCode"] forKey:kCountryCodeKey];
//    // Check if a equal request is not already launched
//    if ([_activeRequests objectForKey:request.requestId] == nil) {
//        [request addRequestEndDelegate:self];
//        ASIHTTPRequest* asiRequest = [self createAsiRequestFromRequest:request];
//        asiRequest.delegate = request;
//        DLog(@"Launching request: %@", asiRequest.url);
//        DLog(@"Request body:\n%@", [[[NSString alloc] initWithData:asiRequest.postBody encoding:NSUTF8StringEncoding] autorelease]);
//        // Display network activity indicator
//        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
//        // Add request to active request array
//        [_activeRequests setObject:asiRequest forKey:request.requestId];
//        // Start request
//        [asiRequest startAsynchronous];
//    } else {
//        DLog(@"Equal request already launched. Ignoring...");
//    }
//    }
    
    /////END///
    
    //Ravi_Bukka: Commented to support new services
    /*   [request addParam:[_communicationStrings stringWithKey:@"countryCode"] forKey:kCountryCodeKey];
     // Check if a equal request is not already launched
     if ([_activeRequests objectForKey:request.requestId] == nil) {
     [request addRequestEndDelegate:self];
     ASIHTTPRequest* asiRequest = [self createAsiRequestFromRequest:request];
     asiRequest.delegate = request;
     DLog(@"Launching request: %@", asiRequest.url);
     DLog(@"Request body:\n%@", [[[NSString alloc] initWithData:asiRequest.postBody encoding:NSUTF8StringEncoding] autorelease]);
     // Display network activity indicator
     [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
     // Add request to active request array
     [_activeRequests setObject:asiRequest forKey:request.requestId];
     // Start request
     [asiRequest startAsynchronous];
     } else {
     DLog(@"Equal request already launched. Ignoring...");
     }
     */
    
}

/** @brief Cancel the selected request */
-(void) cancelRequest:(BaseRequest*)request {
    DLog(@"Cancelling request...");
    ASIHTTPRequest* asiRequest = [_activeRequests objectForKey:request.requestId];
    if (asiRequest != nil) {
        [asiRequest clearDelegatesAndCancel];
        [_activeRequests removeObjectForKey:request.requestId];
    }
}

#pragma mark - RequestEndDelegate
- (void)requestDidFinish:(BaseRequest *)request {
    ASIHTTPRequest* asiRequest = [_activeRequests objectForKey:request.requestId];
    if (asiRequest != nil) {
        [asiRequest clearDelegatesAndCancel];
        [_activeRequests removeObjectForKey:request.requestId];
    }
    
    // Hide network activity indicator when there are no more active requests
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:_activeRequests.count > 0];
}

@end
