//
//  RequestParam.h
//  IMS Digital EPM
//
//  Created by Alex Guti on 27/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RequestParam : NSObject

@property (nonatomic, retain) NSString *key;
@property (nonatomic, retain) NSString *value;

-(id) initWithKey:(NSString*)key andValue:(NSString*)value;

+(RequestParam*) paramWithKey:(NSString*)key andValue:(NSString*)value;

@end
