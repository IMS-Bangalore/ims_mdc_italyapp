//
//  IMSRequest.m
//  IMS Digital EPM
//
//  Created by Diego Prados on 21/12/12.
//
//

#import "IMSRequest.h"
#import "CommunicationStrings.h"

static NSString* const kServerUrlKey = @"serverURL";
static NSString* const kCountryCodeKey = @"country";
static NSString* const kContentWrapperPattern = @"<actionRequest id=\"%@\">%@</actionRequest>";
// Response handle
static NSString* const kResponseXMLKey = @"actionResponse";
static NSString* const kResponseErrorKey = @"result";
static NSInteger const kResponseErrorOKCode = 0;
static NSString* const kBaseRequestErrorDomain  = @"es.lumata.BaseRequest";
static NSString* const kRequestErrorCodeKey = @"RequestErrorCodeKey";

@interface IMSRequest()

@property (nonatomic, retain) CommunicationStrings* communicationStrings;

@end

@implementation IMSRequest

- (id)init
{
    self = [super init];
    if (self) {
        _communicationStrings = [[CommunicationStrings alloc] init];
        // Include country code
        [self addParam:[_communicationStrings stringWithKey:@"countryCode"] forKey:kCountryCodeKey];
    }
    return self;
}

- (void)dealloc
{
    [_communicationStrings release];
    [super dealloc];
}

#pragma mark - Request handle

-(NSString *)url {
    return [_communicationStrings stringWithKey:kServerUrlKey];
    NSLog(@"imsRequest URL %@",[_communicationStrings stringWithKey:kServerUrlKey]);
}

-(NSString *)createBodyContent {
    NSString* paramsString = [super createBodyContent];
    NSString* bodyContent = [NSString stringWithFormat:kContentWrapperPattern, self.name, paramsString];
    
    NSLog(@" body content RRRRRRRR %@", bodyContent);
    return bodyContent;
}

-(ASIHTTPRequest *)createAsiRequest {
    ASIHTTPRequest* asiRequest = [super createAsiRequest];
    // Include auth headers
    [asiRequest addRequestHeader:[_communicationStrings stringWithKey:@"requestAuthUserNameKey"]
                           value:[_communicationStrings stringWithKey:@"requestAuthUserNameValue"]];
    [asiRequest addRequestHeader:[_communicationStrings stringWithKey:@"requestAuthPasswordKey"]
                           value:[_communicationStrings stringWithKey:@"requestAuthPasswordValue"]];
    return asiRequest;
}

#pragma mark - Response handle

-(NSDictionary *)onProcessResponseDictionary:(NSDictionary *)response withError:(NSError **)error {
    NSDictionary* responseDictionary = response;
    if (error) {
        responseDictionary = response[kResponseXMLKey];
        // If response dictionary is nil, notify error
        if (responseDictionary != nil) {
            // Check if request returns error code
            NSInteger errorCode = [[responseDictionary objectForKey:kResponseErrorKey] integerValue];
            if (errorCode != kResponseErrorOKCode) {
                NSString* errorCodeString = [NSString stringWithFormat:@"%d", errorCode];
                NSString* errorDescription = NSLocalizedStringFromTable(errorCodeString, @"ErrorMessages", nil);
                NSMutableDictionary* userInfo = [NSMutableDictionary dictionary];
                [userInfo setObject:errorDescription forKey:NSLocalizedDescriptionKey];
                [userInfo setObject:[NSNumber numberWithInt:errorCode] forKey:kRequestErrorCodeKey];
                *error = [NSError errorWithDomain:kBaseRequestErrorDomain code:errorCode userInfo:userInfo];
            }
        } else {
            *error = [NSError errorWithDomain:kBaseRequestErrorDomain code:90 userInfo:[NSDictionary dictionaryWithObject:NSLocalizedString(@"SERVICE_RESPONSE_ERROR", nil) forKey:NSLocalizedDescriptionKey]];
        }
    }
    return responseDictionary;
}

@end
