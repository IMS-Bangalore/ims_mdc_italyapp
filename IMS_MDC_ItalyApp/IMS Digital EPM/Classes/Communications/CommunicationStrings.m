//
//  CommunicationStrings.m
//  IMS Digital EPM
//
//  Created by Alex Guti on 27/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "CommunicationStrings.h"

@implementation CommunicationStrings

- (id)init {
    self = [super init];
    if (self) {
        _propertyList = [[NSDictionary alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"communications" ofType:@"plist"]];
    }
    return self;
}

- (NSString*)stringWithKey:(NSString*) key {
    return [_propertyList objectForKey:key];
}

- (NSNumber*)numberWithKey:(NSString*) key {
    return [_propertyList objectForKey:key];
}

- (void)dealloc {
    [_propertyList release];
    [super dealloc];
}

@end
