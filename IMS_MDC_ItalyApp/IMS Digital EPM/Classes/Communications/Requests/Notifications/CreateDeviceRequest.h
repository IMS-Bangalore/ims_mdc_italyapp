//
//  CreateDeviceRequest.h
//  IMS Digital EPM
//
//  Created by Diego Prados on 13/12/12.
//
//

#import "Device.h"
#import "RESTJSONRequest.h"
#import "BaseRequest.h"

@interface CreateDeviceRequest : RESTJSONRequest

/** Block that will be executed if login is successful */
typedef void(^CreateDeviceResponseBlock)(Device* device);

/**
 @brief Constructor for CreateDeviceRequest
 @param token Token for getting the device id
 @param deviceAlias (Optional)
 @param onComplete Block that will be executed if the token is correct
 @param onError Block that will be executed if the token is not correct
 */
- (id)initCreateDeviceRequestWithToken:(NSString*)token andDeviceAlias:(NSString*)deviceAlias onComplete:(CreateDeviceResponseBlock)onComplete onError:(RequestErrorBlock)onError;

@end
