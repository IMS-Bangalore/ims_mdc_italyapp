//
//  GetDeviceNotificationsRequest.h
//  IMS Digital EPM
//
//  Created by Diego Prados on 13/12/12.
//
//

#import "Device.h"
#import "RESTJSONRequest.h"
#import "BaseRequest.h"

@interface GetDeviceNotificationsRequest : RESTJSONRequest

/** Block that will be executed if login is successful */
typedef void(^GetDeviceNotificationsResponseBlock)(NSArray* array);

/**
 @brief Constructor for CreateDeviceRequest
 @param device The Device for which we want to get the notifications
 @param onComplete Block that will be executed if we obtain the notifications for the device
 @param onError Block that will be executed if the device is not correct
 */
- (id)initGetDeviceNotificationsRequestWithDevice:(Device*)device onComplete:(GetDeviceNotificationsResponseBlock)onComplete onError:(RequestErrorBlock)onError;


@end
