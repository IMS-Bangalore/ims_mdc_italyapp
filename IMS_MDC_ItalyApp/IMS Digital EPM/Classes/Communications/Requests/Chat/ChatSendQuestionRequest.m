//
//  ChatSendQuestionRequest.m
//  IMS Digital EPM
//
//  Created by Diego Prados on 05/12/12.
//
//

#import "ChatSendQuestionRequest.h"

static NSString* const kDateFormat = @"yyyyMMddHHmmss";

// Error domain
static NSString* const kErrorDomain  = @"es.lumata.ActivateUserRequest";
// Request Name
static NSString* const kRequestName  = @"sendQuestion";
// Content dictionary
//static NSString* const kUserIdKey = @"userId";
static NSString* const kQuestionKey = @"Question";
//static NSString* const kDateKey = @"date";

@implementation ChatSendQuestionRequest

- (id)initChatSendQuestionRequestWithUserId:(NSString *)userId andChatMessage:(ChatMessage*)chatMessage onComplete:(RequestSuccessBlock)onComplete onError:(RequestErrorBlock)onError {
    self = [super init];
    if (self) {
        // Set request name
        self.name = kRequestName;
        // Add request content
//        [self addParam:userId forKey:kUserIdKey];
        [self addParam:chatMessage.message forKey:kQuestionKey];
        
//        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
//        [dateFormatter setDateFormat:kDateFormat];
//        [self addParam:[dateFormatter stringFromDate:chatMessage.date] forKey:kDateKey];
//        [dateFormatter release];
        // Set response handler blocks
        self.onError = onError;
        self.onComplete = ^(NSDictionary* responseDictionary) {
            onComplete();
        };
    }
    return self;
}

@end
