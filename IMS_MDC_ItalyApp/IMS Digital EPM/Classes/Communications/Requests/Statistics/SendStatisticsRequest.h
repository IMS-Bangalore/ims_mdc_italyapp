//
//  SendStatisticsRequest.h
//  IMS Digital EPM
//
//  Created by Diego Prados on 12/12/12.
//
//

#import "StatsEvent.h"
#import "IMSRequest.h"
#import "TwinCodersLibrary.h"

@interface SendStatisticsRequest : IMSRequest

/**
 @brief Constructor for SendStatisticsRequest
 @param userName User id for login
 @param question Question to be sent
 @param onComplete Block that will be executed if login is successful
 @param onError Block that will be executed if login is not successful
 */
- (id)initSendStatisticsRequestWithUserId:(NSString*)userId andStats:(NSArray*)stats onComplete:(TCRequestSuccessBlock)onComplete onError:(TCRequestErrorBlock)onError;

@end
