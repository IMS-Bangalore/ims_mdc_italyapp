//
//  ChangePasswordRequest.h
//  IMS Digital EPM
//
//  Created by Alex Guti on 28/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "BaseRequest.h"

/** @brief Request to change active user password */
@interface ChangePasswordRequest : BaseRequest

/**
 @brief Creates a request to change active user password
 @param userName Entered user name
 @param prevPassword Entered previous password
 @param password New password to be set
 @param onComplete Block that will be executed if request is successful
 @param onError Block that will be executed if request is not successful
 */
-(id) initChangePasswordRequestWithUserName:(NSString*)userName prevPassword:(NSString*)prevPassword password:(NSString*)password onComplete:(RequestSuccessBlock)onComplete onError:(RequestErrorBlock)onError;

@end
