//
//  RemindPasswordRequest.h
//  IMS Digital EPM
//
//  Created by Alex Guti on 28/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "BaseRequest.h"
#import "DoctorInfo.h"

/** @brief Request to remind user password */
@interface RemindPasswordRequest : BaseRequest

/**
 @brief Creates a request to remind user password
 @param doctorInfo User to remind password to
 @param onComplete Block that will be executed if request is successful
 @param onError Block that will be executed if request is not successful
 */
-(id) initRemindPasswordRequestWithDoctorId:(NSString*)doctorId onComplete:(RequestSuccessBlock)onComplete onError:(RequestErrorBlock)onError;

@end
