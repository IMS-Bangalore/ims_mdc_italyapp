//
//  RemindPasswordRequest.m
//  IMS Digital EPM
//
//  Created by Alex Guti on 28/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "RemindPasswordRequest.h"

// Error domain
static NSString* const kErrorDomain  = @"es.lumata.RemindPasswordRequest";
// Request Name
static NSString* const kRequestName  = @"remindPassword";
// Content dictionary
static NSString* const kUserIdKey = @"userId";

@implementation RemindPasswordRequest

-(id) initRemindPasswordRequestWithDoctorId:(NSString*)doctorId onComplete:(RequestSuccessBlock)onComplete onError:(RequestErrorBlock)onError {
    self = [super init];
    if (self) {
        // Set request name
        self.name = kRequestName;
        // Add request content
        [self addParam:doctorId forKey:kUserIdKey];
        // Set response handler blocks
        self.onError = onError;
        self.onComplete = ^(NSDictionary* responseDictionary) {
            onComplete();
        };
    }
    return self;
}

@end