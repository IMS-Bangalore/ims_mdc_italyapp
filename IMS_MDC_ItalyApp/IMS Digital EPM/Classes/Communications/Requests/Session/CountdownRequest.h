//
//  CountdownRequest.h
//  IMS Digital EPM
//
//  Created by Alex Guti on 28/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "BaseRequest.h"
#import "DoctorInfo.h"

/** 
 @brief Block that will be executed when CountdownRequest is successfuly completed
 @param remainingDays Remaining days
 */
typedef void(^CountdownResponseBlock)(NSInteger remainingDays);

/** @brief Request to obtain remaining days to final synchronization */
@interface CountdownRequest : BaseRequest

/**
 @brief Creates a request to obtain remaining days to final synchronization
 @param doctorInfo User to obtain coundown for
 @param onComplete Block that will be executed if request is successful
 @param onError Block that will be executed if request is not successful
 */
-(id) initCountdownRequestWithDoctorInfo:(DoctorInfo*)doctorInfo onComplete:(CountdownResponseBlock)onComplete onError:(RequestErrorBlock)onError;

@end
