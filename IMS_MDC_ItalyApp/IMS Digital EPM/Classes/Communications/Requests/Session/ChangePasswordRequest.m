//
//  ChangePasswordRequest.m
//  IMS Digital EPM
//
//  Created by Alex Guti on 28/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "ChangePasswordRequest.h"

// Error domain
static NSString* const kErrorDomain  = @"es.lumata.ChangePasswordRequest";
// Request Name
static NSString* const kRequestName  = @"changePassword";
// Content dictionary
static NSString* const kUserNameKey =           @"username";
static NSString* const kPrevPasswordKey =       @"oldPassword";
static NSString* const kNewPasswordKey =        @"newPassword";
static NSString* const kConfirmNewPasswordKey = @"confirmNewPassword";

@implementation ChangePasswordRequest

-(id) initChangePasswordRequestWithUserName:(NSString*)userName prevPassword:(NSString*)prevPassword password:(NSString*)password onComplete:(RequestSuccessBlock)onComplete onError:(RequestErrorBlock)onError {
    self = [super init];
    if (self) {
        // Set request name
        self.name = kRequestName;
        // Add request content
        [self addParam:userName forKey:kUserNameKey];
        [self addParam:prevPassword forKey:kPrevPasswordKey];
        [self addParam:password forKey:kNewPasswordKey];
        [self addParam:password forKey:kConfirmNewPasswordKey];
        // Set response handler blocks
        self.onError = onError;
        self.onComplete = ^(NSDictionary* responseDictionary) {
            onComplete();
        };
    }
    return self;
}
@end
