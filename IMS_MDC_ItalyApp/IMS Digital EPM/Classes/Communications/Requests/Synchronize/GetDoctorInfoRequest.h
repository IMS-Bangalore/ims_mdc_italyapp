//
//  GetDoctorInfoRequest.h
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 15/04/13.
//
//

#import "BaseRequest.h"
#import "DoctorInfo.h"
#import "DoctorInfoResponse.h"

/**
 @brief Block that will be called when GetDoctorInfoRequest is successfuly completed
 @param updatedValues YES if Doctor info was updated, false otherwise
 */
typedef void(^GetDoctorInfoResponseBlock)(DoctorInfoResponse* response);

@interface GetDoctorInfoRequest : BaseRequest

/**
 @brief Creates a request to synchronize local doctor info with remote server
 @param syncType Defines the type of synchronization
 @param doctorInfo Doctor info to synchronize
 @param onComplete Block that will be executed if request is successful
 @param onError Block that will be executed if request is not successful
 */
-(id) initGetDoctorInfoRequestWithUserId:(NSString*)userId onComplete:(GetDoctorInfoResponseBlock)onComplete onError:(RequestErrorBlock)onError;

@end
