//
//  MedicamentSync.h
//  IMS Digital EPM
//
//  Created by Alex Guti on 30/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MedicamentSync : NSObject

@property (nonatomic, retain) NSArray *presentations;
@property (nonatomic, retain) NSString *identifier;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *productTypeId;

@end
