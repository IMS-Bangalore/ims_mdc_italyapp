//
//  DiagnosisTypeSync.m
//  IMS Digital EPM
//
//  Created by Alex Guti on 30/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "DiagnosisTypeSync.h"

@implementation DiagnosisTypeSync

@synthesize identifier = _identifier;
@synthesize name = _name;

- (void)dealloc {
    [_identifier release];
    [_name release];
    [super dealloc];
}

-(NSString *)description {
    return [NSString stringWithFormat:@"%@(%@, %@)", self.class, _identifier, _name];
}

@end
