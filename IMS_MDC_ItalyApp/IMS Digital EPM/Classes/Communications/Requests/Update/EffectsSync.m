//
//  EffectsSync.m
//  IMS Digital EPM
//
//  Created by Diego Prados on 22/11/12.
//
//

#import "EffectsSync.h"

@implementation EffectsSync

@synthesize identifier = _identifier;
@synthesize name = _name;

- (void)dealloc {
    [_identifier release];
    [_name release];
    [super dealloc];
}

-(NSString *)description {
    return [NSString stringWithFormat:@"%@(%@, %@)", self.class, _identifier, _name];
}

@end
