//
//  MedicamentSync.m
//  IMS Digital EPM
//
//  Created by Alex Guti on 30/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "MedicamentSync.h"

@implementation MedicamentSync

@synthesize presentations = _presentations;
@synthesize identifier = _identifier;
@synthesize name = _name;
@synthesize productTypeId = _productTypeId;

- (void)dealloc {
    [_presentations release];
    [_identifier release];
    [_name release];
    [_productTypeId release];
    [super dealloc];
}

-(NSString *)description {
    return [NSString stringWithFormat:
            @"%@(%@; %@; %@; Presentations(%d))",
            self.class, _identifier, _name, _productTypeId, _presentations.count];
}

@end
