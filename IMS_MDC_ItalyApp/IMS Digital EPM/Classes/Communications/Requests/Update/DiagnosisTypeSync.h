//
//  DiagnosisTypeSync.h
//  IMS Digital EPM
//
//  Created by Alex Guti on 30/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import <Foundation/Foundation.h>

/** @brief Transport object to insert or update diagnosis type */
@interface DiagnosisTypeSync : NSObject

/** @brief Identifier provided by remote server */
@property (nonatomic, retain) NSString *identifier;
/** @brief Diagnosis name */
@property (nonatomic, retain) NSString *name;

@end
