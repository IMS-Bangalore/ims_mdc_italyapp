//
//  UnitTypeSync.m
//  IMS Digital EPM
//
//  Created by Deepak Carpenter on 28/08/14.
//
//

#import "UnitTypeSync.h"

@implementation UnitTypeSync
@synthesize identifier = _identifier;
@synthesize name = _name;

- (void)dealloc {
    [_identifier release];
    [_name release];
    [super dealloc];
}

-(NSString *)description {
    return [NSString stringWithFormat:@"%@(%@, %@)", self.class, _identifier, _name];
}

@end
