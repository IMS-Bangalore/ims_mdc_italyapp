//
//  RequestLauncher.h
//  IMS Digital EPM
//
//  Created by Alex Guti on 26/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseRequest.h"
#import "CommunicationStrings.h"


/** @brief Object that will handle the launch of the service requests **/
@interface RequestLauncher : NSObject <RequestEndDelegate> {
    // Array of active requests
    NSMutableDictionary* _activeRequests;
    // Communications properties helper
    CommunicationStrings* _communicationStrings;
   
}

/** @brief Launches the selected request */
-(void) launchRequest:(BaseRequest*)request;

/** @brief Cancel the selected request */
-(void) cancelRequest:(BaseRequest*)request;



@end
