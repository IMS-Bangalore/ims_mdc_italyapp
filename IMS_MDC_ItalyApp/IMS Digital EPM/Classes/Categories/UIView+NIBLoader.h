//
//  UIView+NIBLoader.h
//  YUM
//
//  Created by Guillermo Gutiérrez on 28/12/11.
//  Copyright (c) 2011 TwinCoders. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (NIBLoader)
+ (id)loadFromNIB:(NSString*)nibName;
+ (id)loadFromDefaultNIB;
@end
