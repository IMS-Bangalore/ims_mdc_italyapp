//
//  Patient+SortedDiagnosis.m
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 02/04/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "Patient+SortedDiagnosis.h"

@implementation Patient (SortedDiagnosis)
- (NSArray*)sortedDiagnosis {
    NSSortDescriptor* sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"index" ascending:YES];
    return [self.diagnostic sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
}

@end
