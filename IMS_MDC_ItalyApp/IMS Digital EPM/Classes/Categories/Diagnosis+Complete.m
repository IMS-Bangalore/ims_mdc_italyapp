//
//  Diagnosis+Complete.m
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 15/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "Diagnosis+Complete.h"
#import "Treatment+Complete.h"

@implementation Diagnosis (Complete)
- (BOOL)isComplete {
    BOOL isComplete = YES;
    isComplete = isComplete && (self.diagnosisType != nil || self.userDiagnosis.length > 0);
    isComplete = isComplete && self.visitType != nil;
    if (self.needsTreatment.boolValue) {
        isComplete = isComplete && self.treatments.count > 0;
        for (Treatment* treatment in self.treatments) {
            isComplete = isComplete && [treatment isComplete];
        }
    }
    
    return isComplete;
}

- (BOOL)isEmpty {
    BOOL isEmpty = YES;
    isEmpty = isEmpty && (self.diagnosisType == nil && self.userDiagnosis.length == 0);
    isEmpty = isEmpty && self.visitType == nil;
    isEmpty = isEmpty && self.needsTreatment.boolValue;
    isEmpty = isEmpty && self.treatments.count <= 1;
    for (Treatment* treatment in self.treatments) {
        isEmpty = isEmpty && [treatment isEmpty];
    }
    
    return isEmpty;
}
@end
