//
//  UIView+FillWithSubview.h
//  IMS Digital EPM
//
//  Created by Alex Guti on 09/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (FillWithSubview)

/** @brief Method that adds given subview to the view, scalling it to fill container and sets background color to clear */
-(void) fillWithSubview:(UIView *)subview;

@end
