//
//  Treatment+Ready.m
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 15/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "Treatment+Complete.h"
#import "Dosage+Complete.h"

@implementation Treatment (Complete)

- (BOOL)isComplete {
    BOOL isComplete = YES;
    isComplete = isComplete && self.dosage != nil && [self.dosage isComplete];
    isComplete = isComplete && (self.presentation != nil || self.userPresentation.length > 0);
    isComplete = isComplete && (self.medicament != nil || self.userMedicament.length > 0);
    isComplete = isComplete && self.therapyChoiceReason != nil && self.therapyElection != nil;
    
    return isComplete;
}

- (BOOL)isEmpty {
    BOOL isEmpty = YES;
    isEmpty = isEmpty && self.userMedicament.length == 0 && self.medicament == nil;
    isEmpty = isEmpty && self.userPresentation.length == 0;
    isEmpty = isEmpty && self.presentation == nil;
    isEmpty = isEmpty && self.recommendationSpecialist == nil;
    isEmpty = isEmpty && self.replaceReason == nil && self.replacingMedicament == nil;
    isEmpty = isEmpty && self.therapyChoiceReason == nil && self.therapyElection == nil;
    
    return isEmpty;
}

@end
