//
//  Patient+Complete.m
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 15/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "Patient+Complete.h"
#import "Diagnosis+Complete.h"
#import "Age.h"

@implementation Patient (Complete)
- (BOOL)isComplete {
    BOOL isComplete = YES;
    isComplete = isComplete && self.visitDate != nil;
    isComplete = isComplete && self.age.value.intValue > 0 && self.age.ageType != nil;
    isComplete = isComplete && self.consultType != nil;
    isComplete = isComplete && self.gender != nil;
    isComplete = isComplete && self.diagnostic.count > 0;
    isComplete = isComplete && self.recipe != nil;
    for (Diagnosis* diagnosis in self.diagnostic) {
        isComplete = isComplete && [diagnosis isComplete];
    }
    
    return isComplete;
}

- (BOOL)isEmpty {
    BOOL isEmpty = YES;
    isEmpty = isEmpty && self.visitDate == nil;
    isEmpty = isEmpty && self.age.value.intValue == 0;
    isEmpty = isEmpty && self.consultType == nil;
    isEmpty = isEmpty && self.gender == nil;
    isEmpty = isEmpty && self.recipe == nil;
    isEmpty = isEmpty && self.diagnostic.count <= 1;
    for (Diagnosis* diagnosis in self.diagnostic) {
        isEmpty = isEmpty && [diagnosis isEmpty];
    }
    
    return isEmpty;
}

@end
