//
//  Patient+SortedDiagnosis.h
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 02/04/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "Patient.h"

@interface Patient (SortedDiagnosis)
- (NSArray*)sortedDiagnosis;
@end
