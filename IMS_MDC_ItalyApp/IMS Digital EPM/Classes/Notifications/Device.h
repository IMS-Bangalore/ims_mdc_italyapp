//
//  Device.h
//  IMS Digital EPM
//
//  Created by Diego Prados on 13/12/12.
//
//

#import <Foundation/Foundation.h>

@interface Device : NSObject

@property (nonatomic, copy) NSString* deviceId;
@property (nonatomic, copy) NSString* token;
@property (nonatomic, copy) NSString* deviceAlias;
@property (nonatomic, retain) NSDate* creationDate;
@property (nonatomic, retain) NSDate* updateDate;
@property (nonatomic, retain) NSDate* lastRegistrationDate;
@property (nonatomic, copy) NSString* appId;
@property (nonatomic, copy) NSString* type;

@end
