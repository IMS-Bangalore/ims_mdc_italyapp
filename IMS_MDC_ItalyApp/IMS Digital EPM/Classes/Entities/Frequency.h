//
//  Frequency.h
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 09/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Dosage;

@interface Frequency : NSManagedObject

@property (nonatomic, retain) NSString * identifier;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *dosages;
@end

@interface Frequency (CoreDataGeneratedAccessors)

- (void)addDosagesObject:(Dosage *)value;
- (void)removeDosagesObject:(Dosage *)value;
- (void)addDosages:(NSSet *)values;
- (void)removeDosages:(NSSet *)values;
@end
