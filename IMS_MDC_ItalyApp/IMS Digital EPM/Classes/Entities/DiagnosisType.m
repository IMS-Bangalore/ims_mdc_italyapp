//
//  DiagnosisType.m
//  IMS Digital EPM
//
//  Created by Diego Prados on 18/12/12.
//
//

#import "DiagnosisType.h"
#import "Diagnosis.h"


@implementation DiagnosisType

@dynamic identifier;
@dynamic name;
@dynamic deleted;
@dynamic diagnostic;

@end
