//
//  TherapyReplacementReason.h
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 13/11/12.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Treatment;

@interface TherapyReplacementReason : NSManagedObject

@property (nonatomic, retain) NSString * identifier;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *replacementTreatments;
@end

@interface TherapyReplacementReason (CoreDataGeneratedAccessors)

- (void)addReplacementTreatmentsObject:(Treatment *)value;
- (void)removeReplacementTreatmentsObject:(Treatment *)value;
- (void)addReplacementTreatments:(NSSet *)values;
- (void)removeReplacementTreatments:(NSSet *)values;
@end
