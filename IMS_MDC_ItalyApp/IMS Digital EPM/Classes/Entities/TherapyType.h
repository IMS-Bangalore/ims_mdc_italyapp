//
//  TherapyType.h
//  IMS Digital EPM
//
//  Created by Deepak Carpenter on 19/08/14.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Treatment;

@interface TherapyType : NSManagedObject

@property (nonatomic, retain) NSString * identifier;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *therapyReasonTreatments;
@end

@interface TherapyType (CoreDataGeneratedAccessors)

- (void)addTherapyReasonTreatmentsObject:(Treatment *)value;
- (void)removeTherapyReasonTreatmentsObject:(Treatment *)value;
- (void)addTherapyReasonTreatments:(NSSet *)values;
- (void)removeTherapyReasonTreatments:(NSSet *)values;

@end
