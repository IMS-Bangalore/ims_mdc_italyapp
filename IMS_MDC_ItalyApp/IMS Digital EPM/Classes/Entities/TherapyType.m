//
//  TherapyType.m
//  IMS Digital EPM
//
//  Created by Deepak Carpenter on 19/08/14.
//
//

#import "TherapyType.h"
#import "Treatment.h"


@implementation TherapyType

@dynamic identifier;
@dynamic name;
@dynamic therapyReasonTreatments;

@end
