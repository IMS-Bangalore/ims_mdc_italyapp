//
//  Province.m
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 08/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "Province.h"
#import "Doctor.h"


@implementation Province

@dynamic name;
@dynamic identifier;
@dynamic doctors;

@end
