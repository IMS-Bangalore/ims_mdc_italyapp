//
//  AgeType.h
//  IMS Digital EPM
//
//  Created by Deepak Carpenter on 08/05/14.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Age;

@interface AgeType : NSManagedObject

@property (nonatomic, retain) NSString * identifier;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *ages;
@end

@interface AgeType (CoreDataGeneratedAccessors)

- (void)addAgesObject:(Age *)value;
- (void)removeAgesObject:(Age *)value;
- (void)addAges:(NSSet *)values;
- (void)removeAges:(NSSet *)values;

@end
