//
//  PatientInsurance.m
//  IMS Digital EPM
//
//  Created by Nair, Kanchan (Bangalore) on 11/02/14.
//
//

#import "PatientInsurance.h"
#import "Treatment.h"


@implementation PatientInsurance

@dynamic deleted;
@dynamic identifier;
@dynamic name;
@dynamic treatment;

@end
