//
//  Frequency.m
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 09/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "Frequency.h"
#import "Dosage.h"


@implementation Frequency

@dynamic identifier;
@dynamic name;
@dynamic dosages;

@end
