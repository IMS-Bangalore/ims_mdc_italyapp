//
//  Patient.m
//  IMS Digital EPM
//
//  Created by Deepak Carpenter on 04/11/14.
//
//

#import "Patient.h"
#import "Age.h"
#import "ConsultType.h"
#import "Diagnosis.h"
#import "DoctorInfo.h"
#import "Gender.h"
#import "Insurance.h"
#import "PlaceOfVisit.h"
#import "RecipeType.h"
#import "Recurrence.h"
#import "SickFund.h"
#import "Smoker.h"


@implementation Patient

@dynamic index;
@dynamic otherSickFundValue;
@dynamic recurranceValue;
@dynamic sickFundValue;
@dynamic userInsurance;
@dynamic userPlaceOfVisit;
@dynamic userSickFund;
@dynamic visitDate;
@dynamic age;
@dynamic consultType;
@dynamic diagnostic;
@dynamic doctorInfo;
@dynamic gender;
@dynamic insurance;
@dynamic placeOfVisit;
@dynamic recurrence;
@dynamic sickFund;
@dynamic smoker;
@dynamic recipe;

@end
