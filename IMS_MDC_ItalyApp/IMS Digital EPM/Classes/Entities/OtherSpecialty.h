//
//  OtherSpecialty.h
//  IMS Digital EPM
//
//  Created by Deepak Carpenter on 15/12/14.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Doctor, Treatment;

@interface OtherSpecialty : NSManagedObject

@property (nonatomic, retain) NSString * identifier;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *treatments;
@property (nonatomic, retain) Doctor *secondarySpecialityDoctors;
@end

@interface OtherSpecialty (CoreDataGeneratedAccessors)

- (void)addTreatmentsObject:(Treatment *)value;
- (void)removeTreatmentsObject:(Treatment *)value;
- (void)addTreatments:(NSSet *)values;
- (void)removeTreatments:(NSSet *)values;

@end
