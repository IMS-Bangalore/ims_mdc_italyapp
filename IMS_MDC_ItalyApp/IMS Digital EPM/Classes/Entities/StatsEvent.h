//
//  StatsEvent.h
//  IMS Digital EPM
//
//  Created by Diego Prados on 09/01/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface StatsEvent : NSManagedObject

@property (nonatomic, retain) NSNumber * counter;
@property (nonatomic, retain) NSDate * creationDate;
@property (nonatomic, retain) NSDate * endDate;
@property (nonatomic, retain) NSString * event;
@property (nonatomic, retain) NSNumber * sent;
@property (nonatomic, retain) NSDate * sentAtDate;
@property (nonatomic, retain) NSDate * startDate;
@property (nonatomic, retain) NSNumber * timeElapsed;
@property (nonatomic, retain) NSNumber * patientId;

@end
