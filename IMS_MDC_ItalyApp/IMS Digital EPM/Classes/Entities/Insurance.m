//
//  Insurance.m
//  IMS Digital EPM
//
//  Created by Nair, Kanchan (Bangalore) on 11/02/14.
//
//

#import "Insurance.h"
#import "Patient.h"


@implementation Insurance

@dynamic deleted;
@dynamic identifier;
@dynamic name;
@dynamic patient;

@end
