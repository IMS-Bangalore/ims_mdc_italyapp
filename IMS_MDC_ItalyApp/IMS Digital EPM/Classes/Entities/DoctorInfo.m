//
//  DoctorInfo.m
//  IMS Digital EPM
//
//  Created by Deepak Carpenter on 04/11/14.
//
//

#import "DoctorInfo.h"
#import "Doctor.h"
#import "MedicalCenter.h"
#import "Patient.h"
#import "RecentMedicament.h"


@implementation DoctorInfo

@dynamic averageWeeklyPatients;
@dynamic comments;
@dynamic email;
@dynamic firstCollaborationDate;
@dynamic firstCollaborationDateEnd;
@dynamic medicalCenterID;
@dynamic medicalCenterName;
@dynamic otherMedicalCenter;
@dynamic secondCollaborationDate;
@dynamic secondCollaborationDateEnd;
@dynamic weeklyActivityHours;
@dynamic thirdCollaborationDate;
@dynamic thirdCollaborationDateEnd;
@dynamic doctor;
@dynamic medicalCenters;
@dynamic patients;
@dynamic recentMedicaments;

@end
