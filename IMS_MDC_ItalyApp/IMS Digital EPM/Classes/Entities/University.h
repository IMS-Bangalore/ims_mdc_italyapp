//
//  University.h
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 08/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Doctor;

@interface University : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * userDefined;
@property (nonatomic, retain) NSString * identifier;
@property (nonatomic, retain) NSSet *doctors;
@end

@interface University (CoreDataGeneratedAccessors)

- (void)addDoctorsObject:(Doctor *)value;
- (void)removeDoctorsObject:(Doctor *)value;
- (void)addDoctors:(NSSet *)values;
- (void)removeDoctors:(NSSet *)values;
@end
