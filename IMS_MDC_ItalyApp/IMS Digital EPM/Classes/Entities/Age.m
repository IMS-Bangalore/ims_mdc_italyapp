//
//  Age.m
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 08/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "Age.h"
#import "AgeType.h"
#import "Patient.h"


@implementation Age

@dynamic value;
@dynamic ageType;
@dynamic patient;

@end
