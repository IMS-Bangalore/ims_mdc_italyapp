//
//  Age.h
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 08/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class AgeType, Patient;

@interface Age : NSManagedObject

@property (nonatomic, retain) NSNumber * value;
@property (nonatomic, retain) AgeType *ageType;
@property (nonatomic, retain) Patient *patient;

@end
