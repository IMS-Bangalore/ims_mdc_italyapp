//
//  TherapyReplacementReason.m
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 13/11/12.
//
//

#import "TherapyReplacementReason.h"
#import "Treatment.h"


@implementation TherapyReplacementReason

@dynamic identifier;
@dynamic name;
@dynamic replacementTreatments;

@end
