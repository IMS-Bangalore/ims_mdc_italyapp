//
//  Doctor.h
//  IMS Digital EPM
//
//  Created by Deepak Carpenter on 15/12/14.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class DoctorInfo, Gender, MedicalCenter, OtherSpecialty, Province, Specialty, University;

@interface Doctor : NSManagedObject

@property (nonatomic, retain) NSNumber * age;
@property (nonatomic, retain) NSString * graduationYear;
@property (nonatomic, retain) NSString * passwordHash;
@property (nonatomic, retain) NSString * userID;
@property (nonatomic, retain) DoctorInfo *doctorInfo;
@property (nonatomic, retain) Gender *gender;
@property (nonatomic, retain) Specialty *mainSpeciality;
@property (nonatomic, retain) MedicalCenter *medicalCenter;
@property (nonatomic, retain) Province *province;
@property (nonatomic, retain) OtherSpecialty *secondarySpecialty;
@property (nonatomic, retain) University *university;

@end
