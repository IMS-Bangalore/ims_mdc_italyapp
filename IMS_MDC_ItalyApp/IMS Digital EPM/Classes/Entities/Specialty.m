//
//  Specialty.m
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 08/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "Specialty.h"
#import "Doctor.h"


@implementation Specialty

@dynamic name;
@dynamic identifier;
@dynamic mainSpecialityDoctors;
@dynamic secondarySpecialityDoctors;

@end
