//
//  Presentation.m
//  IMS Digital EPM
//
//  Created by Deepak Carpenter on 13/03/14.
//
//

#import "Presentation.h"
#import "Medicament.h"
#import "Treatment.h"
#import "UnitType.h"


@implementation Presentation

@dynamic deleted;
@dynamic identifier;
@dynamic name;
@dynamic medicament;
@dynamic treatments;
@dynamic unitType;

@end
