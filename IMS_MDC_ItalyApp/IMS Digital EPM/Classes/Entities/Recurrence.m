//
//  Recurrence.m
//  IMS Digital EPM
//
//  Created by Deepak Carpenter on 24/02/14.
//
//

#import "Recurrence.h"
#import "Patient.h"


@implementation Recurrence

@dynamic name;
@dynamic identifier;
@dynamic patients;

@end
