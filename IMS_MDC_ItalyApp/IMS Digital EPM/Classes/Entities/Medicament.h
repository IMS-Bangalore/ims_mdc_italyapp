//
//  Medicament.h
//  IMS Digital EPM
//
//  Created by Deepak Carpenter on 13/03/14.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Presentation, ProductType, RecentMedicament, Treatment;

@interface Medicament : NSManagedObject

@property (nonatomic, retain) NSNumber * deleted;
@property (nonatomic, retain) NSString * identifier;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *presentations;
@property (nonatomic, retain) ProductType *productType;
@property (nonatomic, retain) NSSet *recentUses;
@property (nonatomic, retain) Treatment *replacedInTreatments;
@property (nonatomic, retain) NSSet *treatments;
@end

@interface Medicament (CoreDataGeneratedAccessors)

- (void)addPresentationsObject:(Presentation *)value;
- (void)removePresentationsObject:(Presentation *)value;
- (void)addPresentations:(NSSet *)values;
- (void)removePresentations:(NSSet *)values;

- (void)addRecentUsesObject:(RecentMedicament *)value;
- (void)removeRecentUsesObject:(RecentMedicament *)value;
- (void)addRecentUses:(NSSet *)values;
- (void)removeRecentUses:(NSSet *)values;

- (void)addTreatmentsObject:(Treatment *)value;
- (void)removeTreatmentsObject:(Treatment *)value;
- (void)addTreatments:(NSSet *)values;
- (void)removeTreatments:(NSSet *)values;

@end
