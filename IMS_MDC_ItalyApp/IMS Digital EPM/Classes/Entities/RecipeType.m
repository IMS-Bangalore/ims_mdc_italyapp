//
//  RecipeType.m
//  IMS Digital EPM
//
//  Created by Deepak Carpenter on 04/11/14.
//
//

#import "RecipeType.h"
#import "Patient.h"


@implementation RecipeType

@dynamic identifier;
@dynamic name;
@dynamic patients;

@end
