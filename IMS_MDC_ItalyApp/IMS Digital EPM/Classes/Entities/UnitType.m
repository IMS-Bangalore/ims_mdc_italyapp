//
//  UnitType.m
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 09/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "UnitType.h"
#import "Presentation.h"


@implementation UnitType

@dynamic name;
@dynamic identifier;
@dynamic presentations;

@end
