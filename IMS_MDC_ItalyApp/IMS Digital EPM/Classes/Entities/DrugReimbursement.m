//
//  DrugReimbursement.m
//  IMS Digital EPM
//
//  Created by Nair, Kanchan (Bangalore) on 11/02/14.
//
//

#import "DrugReimbursement.h"
#import "Treatment.h"


@implementation DrugReimbursement

@dynamic deleted;
@dynamic identifier;
@dynamic name;
@dynamic treatment;

@end
