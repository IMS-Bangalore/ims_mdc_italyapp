//
//  SessionExpiredHandler.m
//  IMS Digital EPM
//
//  Created by Diego Prados on 16/01/13.
//
//

#import "SessionExpiredHandler.h"

static NSString* const kCommunicationsPlist = @"communications";
static NSString* const kSessionDurationKey = @"sessionDuration";

@interface SessionExpiredHandler()

@property (nonatomic, strong) NSTimer* timer;

@end

@implementation SessionExpiredHandler

#pragma mark SingletonAccess

static SessionExpiredHandler* sharedSessionExpiredHandler = nil;
+ (SessionExpiredHandler*)sharedSessionExpiredHandler {
    if (sharedSessionExpiredHandler == nil) {
        sharedSessionExpiredHandler = [[SessionExpiredHandler alloc] init];
    }
    return sharedSessionExpiredHandler;
}

#pragma mark - Init and dealloc

- (id)init {
    if (( self = [super init] )) {
        
    }
    return self;
}

#pragma mark - Public methods

- (void)startSession {
    [self startCounting];
}

- (void)refreshSession {
    [self startCounting];
}

- (void)checkIfTimerWasFired {
    if ([self.timer.fireDate compare:[NSDate date]] == NSOrderedAscending) {
        [self.timer fire];
    }
}

#pragma mark - Private methods

- (void)startCounting {
    [self stopCounting];
    NSDictionary* communicationsConstants = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:kCommunicationsPlist ofType:@"plist"]];
    int sessionDuration = [[communicationsConstants objectForKey:kSessionDurationKey] intValue] * 60;
    self.timer = [NSTimer scheduledTimerWithTimeInterval:sessionDuration target:self selector:@selector(countdownTracker:) userInfo:nil repeats:NO];
}

- (void)stopCounting {
    [self.timer invalidate];
    self.timer = nil;
}

- (void)countdownTracker:(NSTimer *)theTimer {
    [self stopCounting];
    [self.delegate sessionTimeExpired];
}

#pragma mark - dealloc

- (void)dealloc {
    [_timer invalidate], [_timer release];
    [super dealloc];
}

@end
