//
//  CustomSegmentedControl.h
//  IMS Digital EPM
//
//  Created by Alex Guti on 09/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

@interface CustomSegmentedControl : UISegmentedControl

@property (nonatomic, retain) NSArray *tagArray;
@property (nonatomic, retain) NSArray *values;
@property (nonatomic, retain) NSString *tagIdentifierField;

@property (nonatomic, assign) id selectedValue;

//Utpal: CR#4 Added for Demographic
@property (nonatomic, assign) id customSelectedValue;

@end
