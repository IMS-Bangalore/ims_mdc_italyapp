//
//  RadioButton.h
//  IMS Digital EPM
//
//  Created by Alex Guti on 08/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CheckBox.h"

@interface RadioButton : CheckBox

@end
