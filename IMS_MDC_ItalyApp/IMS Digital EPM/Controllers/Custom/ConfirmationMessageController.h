//
//  ConfirmationMessageController.h
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 12/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import <UIKit/UIKit.h>

@class  ConfirmationMessageController;
@protocol ConfirmationMessageControllerDelegate
- (void)didClickConfirmationButton:(BOOL)accepted;
@end

typedef void (^ConfirmationButtonEventBlock)(BOOL accepted);

@interface ConfirmationMessageController : UIViewController

#pragma mark - Public methods
- (id)initWithMessage:(NSString*)message eventBlock:(ConfirmationButtonEventBlock)eventBlock;
- (id)initWithMessage:(NSString*)message delegate:(id<ConfirmationMessageControllerDelegate>)delegate;

#pragma mark - Properties
@property (nonatomic, retain) NSString* message;
@property (nonatomic, retain) NSString* acceptButtonTitle;
@property (nonatomic, retain) NSString* cancelButtonTitle;
@property (nonatomic, assign) id<ConfirmationMessageControllerDelegate> delegate;
@property (nonatomic, copy) ConfirmationButtonEventBlock eventBlock;

#pragma mark - IBActions
- (IBAction)acceptButtonClicked:(id)sender;
- (IBAction)cancelButtonClicked:(id)sender;

#pragma mark - IBOutlets
@property (retain, nonatomic) IBOutlet UILabel *messageLabel;
@property (retain, nonatomic) IBOutlet UIButton *acceptButton;
@property (retain, nonatomic) IBOutlet UIButton *cancelButton;

@end
