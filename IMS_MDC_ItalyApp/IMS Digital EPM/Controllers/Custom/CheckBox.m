//
//  CheckBox.m
//  IMS Digital EPM
//
//  Created by Alex Guti on 08/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "CheckBox.h"
#import "ComponentGroup.h"

@interface CheckBox()
- (void)addObserverToButtonEvents;
- (void)removeObserverForButtonEvents;
- (void)buttonClicked:(UIButton*)button;
@end

@implementation CheckBox

static NSString* const kDefaultImage = @"checkbox_normal.png";
static NSString* const kSelectedImage = @"checkbox_on.png";
static NSString* const kDisabledImage = @"checkbox_normal.png";

@synthesize checkBoxDelegate = _checkBoxDelegate;
@synthesize value = _value;

#pragma mark Initialization methods

-(void) initCheckBox {
    // Set defaults
    [self setImage:[UIImage imageNamed:kDefaultImage] forState:UIControlStateNormal];
    [self setImage:[UIImage imageNamed:kSelectedImage] forState:UIControlStateSelected];
    [self setImage:[UIImage imageNamed:kDisabledImage] forState:UIControlStateDisabled];
    self.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateDisabled];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [self.titleLabel setFont:[UIFont systemFontOfSize:14]];
    [self setContentMode:UIViewContentModeLeft];
    [self setTitleEdgeInsets:UIEdgeInsetsMake(0, 3, 0, 0)];
    [self addObserverToButtonEvents];
}

- (id)init {
    self = [super init];
    if (self) {
        [self initCheckBox];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        [self initCheckBox];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initCheckBox];
    }
    return self;
}

- (void)dealloc {
    _checkBoxDelegate = nil;
    [_value release];
    [self removeObserverForButtonEvents];
    [super dealloc];
}

- (void)addObserverToButtonEvents {
    [self addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)removeObserverForButtonEvents {
    [self removeTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];    
}

-(void)setSelected:(BOOL)selected {
    if (selected != self.selected) {
        super.selected = selected;
    }
}

-(void)setEnabled:(BOOL)enabled {
    super.enabled = enabled;
    self.alpha = enabled ? 1 : kComponentDisabledAlpha;
}

#pragma mark - Button events
- (void)buttonClicked:(UIButton*)button {
    self.selected = !self.selected;
    [self.checkBoxDelegate checkBoxDidChangeValue:self];
}

@end
