//
//  RadioButtonGroup.m
//  IMS Digital EPM
//
//  Created by Alex Guti on 10/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "RadioButtonGroup.h"

const NSInteger kUnselectedTag = -100;

@implementation RadioButtonGroup
@synthesize buttons = _buttons;
@synthesize values = _values;
@synthesize delegate = _delegate;
@synthesize tagIdentifierField = _tagIdentifierField;


-(void)awakeFromNib {
    for (CheckBox* checkBox in _buttons) {
        checkBox.checkBoxDelegate = self;
    }
}

- (void)dealloc {
    [_buttons release];
    [_values release];
    [_tagIdentifierField release];
    _delegate = nil;
    [super dealloc];
}

-(id)selectedValue {
    
    id returnValue = nil;
    // Obtain checkbox tag
    NSInteger tag = self.selectedTag;
    if (tag != kUnselectedTag) {
        for (id value in _values) {
            if ([[value valueForKey:_tagIdentifierField] integerValue] == tag) {
                returnValue = value;
                break;
            }
        }
    }
    return returnValue;
}

-(void)setSelectedValue:(id)selectedValue {
    
    NSInteger tag = [[selectedValue valueForKey:_tagIdentifierField] integerValue];
    self.selectedTag = tag;
}

-(NSInteger)selectedTag {
    NSInteger tag = kUnselectedTag;
    for (CheckBox* checkBox in _buttons) {
        if (checkBox.selected) {
            tag = checkBox.tag;
            break;
        }
    }
    return tag;
}

-(void)setSelectedTag:(NSInteger)selectedTag {
    // Deselect all buttons
    for (CheckBox* checkBox in _buttons) {
        checkBox.selected = NO;
    }
    // Select checkbox with given tag
    for (CheckBox* checkBox in _buttons) {
        if (checkBox.tag == selectedTag) {
            checkBox.selected = YES;
            break;
        }
    }
}

#pragma mark - CheckBoxDelegate
-(void) checkBoxDidChangeValue:(CheckBox*)aCheckBox {
    if (aCheckBox.selected) {
        for (CheckBox* checkBox in _buttons) {
            if (checkBox != aCheckBox) {
                checkBox.selected = NO;
            }
        }
        [_delegate radioButtonGroupValueChanged:self];
    }
}
@end
