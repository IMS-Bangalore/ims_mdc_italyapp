//
//  LoadingDialogViewController.m
//  IMS Digital EPM
//
//  Created by Alex Guti on 03/04/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "LoadingDialogViewController.h"

@implementation LoadingDialogViewController
@synthesize label = _label;
@synthesize text = _text;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
//Ravi_Bukka
    _text = NSLocalizedString(@"LOADING_MESSAGE", nil);
    _label.text = _text;
}

- (void)viewDidUnload
{
    [self setLabel:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

- (void)dealloc {
    [_label release];
    [_text dealloc];
    [super dealloc];
}

- (void)setText:(NSString *)aText {
    if (_text != aText) {
        [aText retain];
        [_text release];
        _text = aText;
        _label.text = aText;
    }
}


@end
