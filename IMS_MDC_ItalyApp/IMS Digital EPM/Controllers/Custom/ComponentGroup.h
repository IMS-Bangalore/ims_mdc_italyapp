//
//  ComponentGroup.h
//  IMS Digital EPM
//
//  Created by Alex Guti on 08/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import <UIKit/UIKit.h>

extern const CGFloat kComponentDisabledAlpha;

@interface ComponentGroup : UIView

@property (nonatomic, assign, getter=isEnabled) BOOL enabled;
@property (retain, nonatomic) IBOutletCollection(UIView) NSArray *components;
@property (nonatomic, retain) UIColor *enabledBackgroundColor;
@property (nonatomic, retain) UIColor *disabledBackgroundColor;
@end
