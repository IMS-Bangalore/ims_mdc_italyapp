//
//  RadioButtonGroup.h
//  IMS Digital EPM
//
//  Created by Alex Guti on 10/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CheckBox.h"

extern const NSInteger kUnselectedTag;

@class RadioButtonGroup;
@protocol RadioButtonGroupDelegate <NSObject>

-(void)radioButtonGroupValueChanged:(RadioButtonGroup*)buttonGroup;

@end

@interface RadioButtonGroup : NSObject <CheckBoxDelegate>

@property (retain, nonatomic) IBOutletCollection(CheckBox) NSArray *buttons;

@property (nonatomic, retain) NSArray *values;
@property (nonatomic, assign) id selectedValue;
@property (nonatomic, assign) NSInteger selectedTag;
@property (nonatomic, assign) IBOutlet id<RadioButtonGroupDelegate> delegate;
@property (nonatomic, retain) NSString *tagIdentifierField;


@end
