//
//  CustomTextArea.m
//  IMS Digital EPM
//
//  Created by Alex Guti on 09/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "CustomTextArea.h"
#import "UIView+FillWithSubview.h"
#import "ComponentGroup.h"


@implementation CustomTextArea

@synthesize text = _text;
@synthesize placeholder = _placeholder;
@synthesize enabled = _enabled;
@synthesize textArea = _textArea;

#pragma mark Initialization methods

-(void) initialization {
    // Set defaults
    _enabled = YES;
    // Add background image
    UIImage* background = [UIImage imageNamed:@"text_field.png"];
    UIImageView* imageView = [[UIImageView alloc] initWithImage:background];
    [self fillWithSubview:imageView];
    [imageView release];
    // Add text area
    _textArea = [[UIPlaceholderTextArea alloc] init];
    _textArea.font = [UIFont systemFontOfSize:14.f];
    _textArea.backgroundColor = [UIColor clearColor];
    [self fillWithSubview:_textArea];
}

- (id)init {
    self = [super init];
    if (self) {
        [self initialization];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        [self initialization];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initialization];
    }
    return self;
}


-(void)setEnabled:(BOOL)enabled {
    self.alpha = enabled ? 1 : kComponentDisabledAlpha;
    self.textArea.editable = enabled;
}

- (void)setText:(NSString *)aText {
    _textArea.text = aText;
}

- (void)setPlaceholder:(NSString *)aPlaceholder {
    _textArea.placeholder = aPlaceholder;
}

-(NSString *)text {
    return _textArea.text;
}

-(NSString *)placeholder {
    return _textArea.placeholder;
}

- (id<UITextViewDelegate>)delegate
{
    return _textArea.delegate; 
}
- (void)setDelegate:(id<UITextViewDelegate>)delegate {
    _textArea.delegate = delegate;
}

-(BOOL)resignFirstResponder {
    return [_textArea resignFirstResponder];
}



@end
