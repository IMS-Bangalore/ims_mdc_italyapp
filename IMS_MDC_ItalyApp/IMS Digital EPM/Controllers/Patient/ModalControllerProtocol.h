//
//  ModalControllerProtocol.h
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 22/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ModalControllerProtocol <NSObject>
- (void)presentModalController:(UIViewController*)controller;
- (void)dismissModalController:(UIViewController*)controller;
@end
