//
//  PlaceOfVisitTableViewController.m
//  IMS Digital EPM
//
//  Created by Bukka, Ravi (Bangalore) on 20/12/13.
//
//

#import "PlaceOfVisitTableViewController.h"
//#import "PatientViewController.h"
@interface PlaceOfVisitTableViewController ()
@end


@implementation PlaceOfVisitTableViewController

@synthesize placeOfVisitArray = _placeOfVisitArray;
//@synthesize patientViewController = _patientViewController;
@synthesize delegate = _delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _tableView.delegate = self;
    _tableView.dataSource = self;
    
    if([[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0] isEqualToString:@"el"]) {
  
        _placeOfVisitArray = [[NSMutableArray alloc] init];
                [_placeOfVisitArray addObject:NSLocalizedString(@"PVT_SURGERY", nil)];
                [_placeOfVisitArray addObject:NSLocalizedString(@"SICK_FUND_SURGERY", nil)];
                [_placeOfVisitArray addObject:NSLocalizedString(@"PUBLIC_HOSPITAL", nil)];
                [_placeOfVisitArray addObject:NSLocalizedString(@"PVT_HOSPITAL", nil)];
                [_placeOfVisitArray addObject:NSLocalizedString(@"HEALTH_CENTER", nil)];
                [_placeOfVisitArray addObject:NSLocalizedString(@"PATIENT_RESIDENCE", nil)];
                [_placeOfVisitArray addObject:NSLocalizedString(@"BY_PHONE", nil)];
                [_placeOfVisitArray addObject:NSLocalizedString(@"OTHER", nil)];
        

        
    }

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
    return [_placeOfVisitArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    // Configure the cell...
    
    cell.textLabel.text = [_placeOfVisitArray objectAtIndex:indexPath.row];
    cell.textLabel.font = [UIFont systemFontOfSize:14.0];
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
    NSString *cellText = selectedCell.textLabel.text;
    
    
  
[_delegate placeOfVisitDismissPopOverController:self didSelectTableView:tableView rowAtIndexPath:indexPath didSelectString:cellText];
    
}

@end
