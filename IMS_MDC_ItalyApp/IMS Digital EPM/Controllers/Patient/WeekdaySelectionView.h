//
//  WeekdaySelectionView.h
//  IMS Digital EPM
//
//  Created by Diego Prados on 08/10/12.
//
//

#import <UIKit/UIKit.h>
#import "TwinCodersLibrary.h"
#import "WeekdayButton.h"

@protocol WeekdaySelectionViewDelegate <NSObject>

/**
 * @brief Mark the day of the visit
 */
- (void)selectVisitDay:(NSDate*)date;

@end

@interface WeekdaySelectionView : UIView <WeekdayButtonDelegate>

@property (retain, nonatomic) IBOutletCollection(UIView) NSArray *weekdayButtons;
@property (nonatomic, assign) IBOutlet id<WeekdaySelectionViewDelegate> delegate;
@property (nonatomic, retain) NSDate *firstDate;
@property (nonatomic, retain) NSDate *secondDate;

- (void)dateSelected:(NSInteger)day;

@end
