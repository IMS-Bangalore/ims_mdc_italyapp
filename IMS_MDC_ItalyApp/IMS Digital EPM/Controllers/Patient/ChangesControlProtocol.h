//
//  ChangesControlProtocol.h
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 22/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import <Foundation/Foundation.h>

/** @param accepted YES if the changes are accepted, NO otherwise */
typedef void (^ChangesControlBlock)(BOOL accepted);

@protocol ChangesControlProtocol <NSObject>
/**
 * @brief Asks for permissions to make changes, as the operation may be 
 *      asynchronous, a block for the callback is provided 
 */
- (void)shouldEditEntity:(Synchronizable*)entity withControlBlock:(ChangesControlBlock)controlBlock;
- (void)shouldEditEntityLosingChanges:(Synchronizable*)entity withControlBlock:(ChangesControlBlock)controlBlock;

@end
