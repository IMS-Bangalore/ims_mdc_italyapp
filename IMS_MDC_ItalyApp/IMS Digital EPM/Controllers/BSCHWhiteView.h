//
//  WhiteView.h
//  IMS Digital EPM
//
//  Created by Diego Prados on 18/10/12.
//
//

#import <UIKit/UIKit.h>

@interface BSCHWhiteView : UIView

@property (nonatomic) CGRect buttonFrame;

- (id)initWithFrame:(CGRect)frame andButtonFrame:(CGRect)buttonFrame;
- (void)setButtonFrame:(CGRect)buttonFrame;

@end
