//
//  ExpandedMedicamentCell.h
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 11/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Treatment.h"
#import "TreatmentViewController.h"
#import "ChangesControlProtocol.h"

@class ExpandedMedicamentCell;
@protocol ExpandedMedicamentCellDelegate <ModalControllerProtocol>
- (void)expandedMedicamentCellCollapseButtonClicked:(ExpandedMedicamentCell*)cell;
- (void)expandedMedicamentCellDeleteButtonClicked:(ExpandedMedicamentCell*)cell;
- (void)expandedMedicamentCell:(ExpandedMedicamentCell*)cell willPresentPopover:(UIPopoverController*)popoverController;
- (void)expandedMedicamentCellIsReady:(BOOL)ready;
@end

@interface ExpandedMedicamentCell : UITableViewCell<TreatmentViewControllerDelegate>

#pragma mark - Properties
@property (nonatomic, assign, getter=isEnabled) BOOL enabled;
@property (nonatomic, retain) NSString *cellTitle;
@property (nonatomic, retain) Treatment* treatment;
@property (nonatomic, assign) id<ExpandedMedicamentCellDelegate> delegate;
@property (nonatomic, retain) id<ChangesControlProtocol> changesControlProtocol;

#pragma mark - IBOutlets
@property (retain, nonatomic) IBOutlet UIView *treatmentPlaceholderView;
@property (retain, nonatomic) IBOutlet UIButton *collapseButton;
@property (retain, nonatomic) IBOutlet UIButton *deleteButton;

#pragma mark - IBActions
- (IBAction)collapseButtonClicked:(id)sender;
- (IBAction)deleteButtonClicked:(id)sender;

#pragma mark - Public methods
- (BOOL)requestDismiss;

@end
