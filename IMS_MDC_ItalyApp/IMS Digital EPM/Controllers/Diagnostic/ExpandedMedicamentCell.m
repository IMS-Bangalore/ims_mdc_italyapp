//
//  ExpandedMedicamentCell.m
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 11/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "ExpandedMedicamentCell.h"

@interface ExpandedMedicamentCell()
@property (nonatomic, retain) TreatmentViewController* treatmentViewController;
@end

@implementation ExpandedMedicamentCell
@synthesize cellTitle = _cellTitle;
@synthesize treatment = _treatment;
@synthesize treatmentPlaceholderView = _treatmentPlaceholderView;
@synthesize collapseButton = _collapseButton;
@synthesize deleteButton = _deleteButton;
@synthesize treatmentViewController = _treatmentViewController;
@synthesize delegate = _delegate;
@synthesize enabled = _enabled;
@synthesize changesControlProtocol = _changesControlProtocol;


#pragma mark - Init and dealloc
- (void)initializeCell {
    if (_treatmentViewController == nil) {
        _treatmentViewController = [[TreatmentViewController alloc] initWithNibName:@"TreatmentViewController" bundle:nil];
        _treatmentViewController.delegate = self;
        _treatmentViewController.changesControlProtocol = _changesControlProtocol;
        CGSize size = _treatmentPlaceholderView.frame.size;
        _treatmentViewController.view.frame = CGRectMake(0, 0, size.width, size.height);
        
        [_treatmentPlaceholderView addSubview:_treatmentViewController.view];
        _treatmentViewController.treatment = _treatment;
    }
}

- (void)awakeFromNib {
    [self initializeCell];
}

- (void)dealloc {
    [_treatmentPlaceholderView release];
    [_collapseButton release];
    [_treatmentViewController release];
    [_deleteButton release];
    [_cellTitle release];
    [_treatment release];
    _delegate = nil;
    [super dealloc];
}

- (void)setCellTitle:(NSString *)cellTitle {
    [cellTitle retain];
    [_cellTitle release];
    _cellTitle = cellTitle;
    
    [_collapseButton setTitle:cellTitle forState:UIControlStateNormal];
}

- (void)setTreatment:(Treatment *)treatment {
    [treatment retain];
    [_treatment release];
    _treatment = treatment;
    
    _treatmentViewController.treatment = _treatment;
}

- (void)setEnabled:(BOOL)flag {
    _enabled = flag;
    _treatmentViewController.enabled = flag;
    _collapseButton.enabled = flag;
    _deleteButton.enabled = flag;
}

- (void)setChangesControlProtocol:(id)changesControlProtocol {
    _changesControlProtocol = changesControlProtocol;
    _treatmentViewController.changesControlProtocol = _changesControlProtocol;
}


#pragma mark - IBActions
- (IBAction)collapseButtonClicked:(id)sender {
    [_delegate expandedMedicamentCellCollapseButtonClicked:self];
}

- (IBAction)deleteButtonClicked:(id)sender {
    [_delegate expandedMedicamentCellDeleteButtonClicked:self];
}

#pragma mark - TreatmentViewControllerDelegate
-(void) treatmentViewController:(TreatmentViewController*)controller isReady:(BOOL)ready {
    [_delegate expandedMedicamentCellIsReady:ready];
}

- (void)treatmentViewController:(TreatmentViewController *)controller willPresentPopover:(UIPopoverController *)popoverController {
    [_delegate expandedMedicamentCell:self willPresentPopover:popoverController];
}

#pragma mark - ModalControllerProtocol
- (void)presentModalController:(UIViewController*)controller {
    [_delegate presentModalController:controller];
}

- (void)dismissModalController:(UIViewController*)controller {
    [_delegate dismissModalController:controller];
}

#pragma mark - Public methods
- (BOOL)requestDismiss {
    return [_treatmentViewController requestDismiss];
}

@end
