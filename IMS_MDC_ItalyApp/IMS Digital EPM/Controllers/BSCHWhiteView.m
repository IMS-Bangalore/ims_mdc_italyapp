//
//  WhiteView.m
//  IMS Digital EPM
//
//  Created by Diego Prados on 18/10/12.
//
//

#import "BSCHWhiteView.h"

@implementation BSCHWhiteView

@synthesize buttonFrame = _buttonFrame;

- (id)initWithFrame:(CGRect)frame andButtonFrame:(CGRect)buttonFrame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        _buttonFrame = buttonFrame;
        self.opaque = NO;
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
    // Start by filling the area with the blue color
    [[UIColor colorWithRed:1 green:1 blue:1 alpha:0.7] setFill];
    UIRectFill( rect );
    
    // Assume that there's an ivar somewhere called holeRect of type CGRect
    // We could just fill holeRect, but it's more efficient to only fill the
    // area we're being asked to draw.
    
    CGRect holeRectIntersection = CGRectIntersection( _buttonFrame, rect );

    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetBlendMode(context, kCGBlendModeClear);
    [[UIColor clearColor] set];
    UIRectFill( holeRectIntersection );
}

- (void)setButtonFrame:(CGRect)buttonFrame {
    _buttonFrame = buttonFrame;
    [self setNeedsDisplay];
}

@end
