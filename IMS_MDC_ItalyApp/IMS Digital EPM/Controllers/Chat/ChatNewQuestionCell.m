//
//  ChatNewQuestionCell.m
//  IMS Digital EPM
//
//  Created by Diego Prados on 10/12/12.
//
//

#import "ChatNewQuestionCell.h"

@implementation ChatNewQuestionCell

@synthesize questionLabel;

#pragma mark - Init

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        questionLabel.text = NSLocalizedString(@"ENTER_NEW_QUESTION...", nil);
    }
    return self;
}


#pragma mark - Setters

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - IBActions

- (IBAction)openQuestionComposer:(id)sender {
    [self.delegate openMessageComposer];
}

@end
