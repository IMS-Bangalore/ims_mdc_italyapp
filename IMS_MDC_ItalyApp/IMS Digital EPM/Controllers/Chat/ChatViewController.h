//
//  ChatViewController.h
//  IMS Digital EPM
//
//  Created by Diego Prados on 05/12/12.
//
//

#import <UIKit/UIKit.h>
#import "ChatNewQuestionCell.h"
#import "ChatMessageComposerViewController.h"
#import "ModalControllerProtocol.h"

//Ravi_Bukka:Added for logging feature
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "PatientViewController.h"
//Deepak_Carpenter:Added new reachability class
#import "Reachability.h"

@class ChatViewController;
@protocol ChatViewControllerDelegate <ModalControllerProtocol>

- (void)chatControllerDidFinishLoading:(ChatViewController*)chatController;

@end

@interface ChatViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, ChatNewQuestionCellDelegate, ChatMessageComposerViewControllerDelegate, MFMailComposeViewControllerDelegate>
{
    //Kanchan : Added for CR#6
    Reachability* internetReachable;

}

#pragma mark - Properties
@property (nonatomic, retain) NSMutableArray* messages;
@property (nonatomic, copy) NSString* userId;
@property (nonatomic, assign) id<ChatViewControllerDelegate> delegate;

//Kanchan: CR#6 send chat message when log button clicked
@property (nonatomic, assign) id<ChatMessageComposerViewControllerDelegate> newDelegate;

//Deepak_Carpenter : added for CR#6 send chat message when log button clicked
-(void)sendconfirmationMessgae;

//Ravi_Bukka:Added for logging feature
@property (nonatomic, retain) PatientViewController *patientViewController;

#pragma mark - IBOutlets
@property (retain, nonatomic) IBOutlet UITableView *tableView;

#pragma mark - Public methods
- (void)loadMessages;
- (NSInteger)numberOfUnreadMessagesForDate:(NSDate*)lastDate;

#pragma mark - IBActions
- (IBAction)refreshMessages:(id)sender;

//Ravi_Bukka:Added for logging feature
- (IBAction)loggingPatientDetails:(id)sender;

@end
