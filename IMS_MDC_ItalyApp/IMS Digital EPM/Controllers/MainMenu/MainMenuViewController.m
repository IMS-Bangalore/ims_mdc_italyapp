//
//  MainMenuViewController.m
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 02/10/12.
//
//

#import "MainMenuViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "TwinCodersLibrary.h"
#include "easing.h"

typedef enum {
    kAnimationStop,
    kAnimationStart,
    kAnimationDragging,
    kAnimationDecelerating
} AnimationStatus;

#pragma mark - Class Extension
@interface MainMenuViewController ()

#pragma mark Menu items
@property (retain, nonatomic) NSArray *menuItemViews;

#pragma mark Animation status
@property (nonatomic, assign) AnimationStatus animationStatus;
@property (nonatomic, assign) CGFloat currentPosition;
@property (nonatomic, assign) CGPoint lastDragPosition;

@end

@implementation MainMenuViewController
@synthesize delegate = _delegate;
@synthesize menuItems = _menuItems;
@synthesize animationStatus = _animationStatus;
@synthesize lastDragPosition = _lastDragPosition;
@synthesize menuContentView = _menuContentView;
@synthesize panGestureRecognizer = _panGestureRecognizer;
@synthesize numberOfUnreadMessages = _numberOfUnreadMessages;


#pragma mark Animation constants
static const CGFloat kMinMenuItemProportion = 0.4;
static const CGFloat kDragVelocity = 1.0;
static const NSTimeInterval kAnimationDuration = 3.0;
static const CGFloat kAnimationAngleStep = 0.01;
static const NSInteger kAnimationMaxSteps = 1.0 / 0.02;
static const CGFloat kVelocityNormalizationFactor = 1 / 6000.0;
static const CGFloat kDurationNormalizationFactor = 4.0;
static const CGFloat kStopVelocityThreshold = 300.0;

static const CGFloat kAngleNormalizationFactor = 0.82;

static NSString* const kTransformAnimation = @"transformAnimation";

typedef double (^TimingFunction)(double time);


#pragma mark - Init and dealloc
static NSArray* observedValues = nil;
- (void)initialize {
    if (observedValues == nil) {
        observedValues = [[NSArray alloc] initWithObjects:@"menuItems", @"animationStatus", nil];
    }
    
    for (NSString* observedKey in observedValues) {
        [self addObserver:self forKeyPath:observedKey options:NSKeyValueObservingOptionNew context:nil];
    }
}

- (id)init {
    self = [super init];
    if (self) {
        [self initialize];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (( self = [super initWithCoder:aDecoder] )) {
        [self initialize];
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (( self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        [self initialize];
    }
    return self;
}

- (void)dealloc {
    for (NSString* observedKey in observedValues) {
        [self removeObserver:self forKeyPath:observedKey];
    }
    _delegate = nil;
    [_menuItems release];
    [_panGestureRecognizer release];
    [_menuContentView release];
    [_menuItemViews release];
    [super dealloc];
}

#pragma mark - Public methods

- (void)refreshItemWithBadgeCount:(NSInteger)badgeCount andMenuItemId:(NSInteger)identifier {
    for (MainMenuItem* menuItem in self.menuItems) {
        if (menuItem.identifier == identifier) {
            menuItem.badgeCount = badgeCount;
        }
    }
}

#pragma mark - Angle conversion methods
- (CGFloat)toRealAngle:(CGFloat)normalizedAngle {
    CGFloat angle = fmodf(10.0f + normalizedAngle, 1.0f);
    if (angle < 0.5) {
        angle = powf(angle / 0.5, kAngleNormalizationFactor) * 0.5;
    }
    else {
        angle = 1 - powf(1 - ((angle - 0.5) / 0.5), kAngleNormalizationFactor) * 0.5;
    }
    return 2 * M_PI * angle;
}

- (CGFloat)toNormalizedAngle:(CGFloat)realAngle {
    // Lo pasamos al intervalo [0, 1]
    CGFloat angle = realAngle / (2 * M_PI);
    angle = fmodf(10.0f + angle, 1.0f);
    if (angle < 0.5) {
        // Calculamos la inversa
        angle = powf(angle * 2, 1 / kAngleNormalizationFactor);
        // Dividimos entre dos para tener el ángulo definitivo
        angle = angle / 2;
    } else {
        // Lo pasamos a [1, 0]
        angle = 1 - ((angle - 0.5) * 2);
        // Calculamos la inversa
        angle = 1 - powf(angle, 1 / kAngleNormalizationFactor);
        // Obtenemos el ángulo definitivo
        angle = 0.5 + (angle / 2);
    }
    return angle;
}

#pragma mark - KVO events
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if ([keyPath isEqualToString:@"menuItems"]) {
        [self reloadUI];
    }
    else if ([keyPath isEqualToString:@"animationStatus"]) {
        [self updateMenuItemsInteraction];
    }
    else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

#pragma mark - Menu item methods
- (void)updateMenuItemsInteraction {
    DAssert(_menuItemViews.count > 0, @"No menu items have been defined!");
    
    BOOL buttonsEnabled = _animationStatus == kAnimationStop;
    for (MainMenuElementView* view in _menuItemViews) {
        view.userInteractionEnabled = buttonsEnabled;
    }
    
    NSInteger selectedViewIndex = (NSInteger)roundf(_menuItemViews.count * (1.0 - _currentPosition)) % _menuItemViews.count;
    MainMenuElementView* selectedView = _animationStatus == kAnimationStop ? [_menuItemViews objectAtIndex:selectedViewIndex] : nil;
    for (MainMenuElementView* menuItemView in _menuItemViews) {
        [menuItemView setButtonSelected:menuItemView == selectedView animated:YES];
    }
}

- (void)reloadUI {
    for (UIView* view in _menuItemViews) {
        [view removeFromSuperview];
    }
    self.menuItemViews = nil;
    
    NSMutableArray* menuItemViews = [NSMutableArray arrayWithCapacity:_menuItems.count];
    for (MainMenuItem* menuItem in _menuItems) {
        MainMenuElementView* menuItemView = [MainMenuElementView loadFromDefaultNIB];
        menuItemView.menuItem = menuItem;
        menuItemView.delegate = self;
        [_menuContentView addSubview:menuItemView];
        menuItemView.center = CGPointZero; // Place all view centers in 0,0 . They are moved using CALayer transforms
        
        [menuItemViews addObject:menuItemView];
    }
    self.menuItemViews = menuItemViews;
    
    if (_menuItemViews.count > 0) {
        [self setCurrentPosition:_currentPosition];
    }
    self.animationStatus = kAnimationStop;
}

#pragma mark - Scale and Position Transform methods
- (CGSize)availableMenuSize {
    return _menuContentView.frame.size;
}

- (CATransform3D)transformFromAngle:(CGFloat)angle {
    CGSize availableSize = [self availableMenuSize];
    CGFloat positionX = (availableSize.width - (sinf([self toRealAngle:angle]) * availableSize.width)) / 2;
    CGFloat positionY = (availableSize.height - (cosf([self toRealAngle:angle] + M_PI) * availableSize.height)) / 2;
    CGFloat scale = [self sizeProportionFromAngle:angle];
    
    CATransform3D scaleTransform = CATransform3DMakeScale(scale, scale, scale);
    CATransform3D translationTransform = CATransform3DMakeTranslation(positionX, positionY, scale);
    return CATransform3DConcat(scaleTransform, translationTransform);
}

- (CGFloat)convertToAngleFromPosition:(CGPoint)position {
    CGSize availableSize = [self availableMenuSize];
    CGFloat angleSin = (availableSize.width - (position.x * 2)) / availableSize.width;
    CGFloat angleCos = (availableSize.height - (position.y * 2)) / availableSize.height;
    
    CGFloat angleFromSin = asinf(angleSin);
    
    // Set angle based on quadrant
    CGFloat realAngle;
    if (angleCos >= 0) {
        realAngle = - angleFromSin + M_PI;
    }else {
        realAngle = angleFromSin;
    }
    
    CGFloat angle = fmodf([self toNormalizedAngle:realAngle] + 1.0f, 1.0);
    return angle;
}

- (CGFloat)sizeProportionFromAngle:(CGFloat)angle {
    CGFloat sizeProportion = (((1 + cosf([self toRealAngle:angle])) / 2) * (1.0f - kMinMenuItemProportion)) + kMinMenuItemProportion;
    
    return sizeProportion;
}

#pragma mark - Animation util methods
- (BOOL)isShortestPathClockwiseFromAngle:(CGFloat)fromAngle toAngle:(CGFloat)toAngle {
    CGFloat deltaAngle = toAngle - fromAngle;
    BOOL clockwise = fabsf(deltaAngle) < 0.5;
    if (deltaAngle < 0) {
        // Delta angle is negative, reverse the direction
        clockwise = !clockwise;
    }
    return clockwise;
}

#pragma mark - Animation methods
- (void)cancelAnimations {
    UIView* keyView = [_menuItemViews objectAtIndex:0];
    if ([keyView.layer animationForKey:kTransformAnimation] != nil) {
        CALayer* presentationLayer = keyView.layer.presentationLayer;
        
        for (UIView* menuItemView in _menuItemViews) {
            [menuItemView.layer removeAnimationForKey:kTransformAnimation];
        }
        
        CGFloat positionX = [[presentationLayer valueForKeyPath:@"transform.translation.x"] floatValue];
        CGFloat positionY = [[presentationLayer valueForKeyPath:@"transform.translation.y"] floatValue];
        CGPoint position = CGPointMake(positionX, positionY);
        CGFloat currentAngle = [self convertToAngleFromPosition:position];
        [self setCurrentPosition:currentAngle];
    }
}

- (void)alignViewsAnimated:(BOOL)animated {
    NSInteger viewsCount = _menuItemViews.count;
    CGFloat targetAngle = roundf(_currentPosition * viewsCount) / viewsCount;
    
    [self setCurrentPosition:targetAngle animated:animated];
}

- (void)setAngle:(CGFloat)angle fromAngle:(CGFloat)fromAngle toView:(MainMenuElementView*)view animated:(BOOL)animated {
    
    BOOL clockwise = [self isShortestPathClockwiseFromAngle:fromAngle toAngle:angle];
    CGFloat normalizedFromAngle = fromAngle;
    CGFloat normalizedToAngle = angle;
    if (clockwise && normalizedFromAngle > normalizedToAngle) {
        normalizedToAngle = normalizedToAngle + 1;
    }
    if (!clockwise && normalizedFromAngle < normalizedToAngle) {
        normalizedFromAngle = normalizedFromAngle + 1;
    }
    CGFloat deltaAngle = normalizedToAngle - normalizedFromAngle;
    
    // Define custom timing function
    TimingFunction timingFunction = ^double (double time) {
        static const double separation = 0.7;
        double result;
        if (time < separation) {
            result = CubicEaseIn(time / separation) * separation;
        }
        else {
            result = (CubicEaseIn(1) * separation) + (BackEaseOut((time - separation) / (1 - separation)) * (1 - separation));
        }
        return result;
    };
    
    CGFloat duration = animated ? fabs(deltaAngle) * kAnimationDuration : 0;
    
    
    [CATransaction begin];
    if (animated) {
        self.animationStatus = kAnimationDecelerating;
        [CATransaction setCompletionBlock:^{
            // Update animation status
            if (_animationStatus == kAnimationDecelerating) {
                self.animationStatus = kAnimationStop;
            }
        }];
    }
    
    [self animateView:view fromAngle:normalizedFromAngle toAngle:normalizedToAngle duration:duration timingFunction:timingFunction];
    
    [CATransaction commit];
}

- (void)animateDecelerationWithVelocity:(CGFloat)initialVelocity {
    if (fabs(initialVelocity) > kStopVelocityThreshold) {
        CGFloat fromAngle = _currentPosition;
        CGFloat angleDistance = - initialVelocity * kVelocityNormalizationFactor;
        CGFloat toAngle = fromAngle + angleDistance;
        CGFloat angleOffset = 1.0 / _menuItemViews.count;
        CGFloat duration = log10f(fabs(initialVelocity));
        
        TimingFunction decelerationTimingFunction = ^double(double time){
//            return (1.0 - pow((1.0 - time), 6));
            return ExponentialEaseOut(time);
        };
        
        _currentPosition = fmodf(10.0 + toAngle, 1.0);
        
        [CATransaction begin];
        [CATransaction setCompletionBlock:^{
            // Align views only if the user is not touching the screen
            if (_animationStatus == kAnimationDecelerating) {
                [self alignViewsAnimated:YES];
            }
        }];
        
        for (UIView* menuItemView in _menuItemViews) {
            [self animateView:menuItemView
                    fromAngle:fromAngle
                      toAngle:toAngle
                     duration:duration
               timingFunction:decelerationTimingFunction];
            fromAngle += angleOffset;
            toAngle += angleOffset;
        }
        
        [CATransaction commit];
    }
    else {
        [self alignViewsAnimated:YES];
    }
}

- (void)animateView:(UIView*)view fromAngle:(CGFloat)fromAngle toAngle:(CGFloat)toAngle duration:(CGFloat)duration timingFunction:(TimingFunction)timingFunction {
    
    NSInteger steps = duration == 0 ? 0 : fabs(toAngle - fromAngle) / kAnimationAngleStep;
    
    // If not animated, simply move and scale
    view.layer.transform = [self transformFromAngle:toAngle];
    
    CAAnimation* transformAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform" function:^NSValue *(double time) {
        CGFloat angle = fromAngle + (timingFunction(time) * (toAngle - fromAngle));
//        return [NSNumber numberWithFloat:[self sizeProportionFromAngle:angle]];
        return [NSValue valueWithCATransform3D:[self transformFromAngle:angle]];
    } steps:steps];
    transformAnimation.duration = duration;
    [view.layer addAnimation:transformAnimation forKey:kTransformAnimation];
}

- (void)setCurrentPosition:(CGFloat)currentPosition animated:(BOOL)animated {
    DAssert(_menuItemViews.count > 0, @"No menu items have been defined!");
    
    [self cancelAnimations];
    
    CGFloat previousPosition = _currentPosition;
    _currentPosition = fmodf(1.0 + currentPosition, 1.0);
    
    CGFloat fromViewPosition = previousPosition;
    CGFloat toViewPosition = _currentPosition;
    CGFloat angleOffset = 1.0 / _menuItemViews.count;
    
    for (MainMenuElementView* menuItemView in _menuItemViews) {
        [self setAngle:toViewPosition fromAngle:fromViewPosition toView:menuItemView animated:animated];
        fromViewPosition = fmodf(fromViewPosition + angleOffset, 1.0f);
        toViewPosition = fmodf(toViewPosition + angleOffset, 1.0f);
    }
}

- (void)setCurrentPosition:(CGFloat)currentPosition {
    [self setCurrentPosition:currentPosition animated:NO];
}

- (void)animateDistance:(CGFloat)distance {
    CGFloat deltaAngle = kDragVelocity * distance / (M_PI * [self availableMenuSize].width / 2);
    [self setCurrentPosition:_currentPosition - deltaAngle animated:NO];
}

#pragma mark - IBActions
- (IBAction)gestureRecognizerDidDetectEvent:(UIPanGestureRecognizer *)sender {
//    DLog(@"Gesture recognizer event %d", sender.state);
    
    CGPoint translatedPoint = [sender translationInView:self.view];
    
    switch (sender.state) {
        case UIGestureRecognizerStateBegan: {
            self.animationStatus = kAnimationStart;
            
            DAssert(_menuItemViews.count > 0, @"No menu items have been defined!");
            
            [self cancelAnimations];
            
            _lastDragPosition = translatedPoint;
            break;
        }
        case UIGestureRecognizerStateCancelled:
        case UIGestureRecognizerStateEnded: {
            self.animationStatus = kAnimationDecelerating;
            CGFloat velocity = [sender velocityInView:self.view].x;
            DLog(@"Gesture ended with velocity: %.4f", velocity);
            [self animateDecelerationWithVelocity: velocity];
            break;
        }
        case UIGestureRecognizerStateChanged: {
            if (_animationStatus != kAnimationDragging) {
                self.animationStatus = kAnimationDragging;
            }
            
            DAssert(_menuItemViews.count > 0, @"No menu items have been defined!");
            
            CGFloat distance = translatedPoint.x - _lastDragPosition.x;
            [self animateDistance:distance];
            self.lastDragPosition = translatedPoint;
            break;
        }
        default:
            break;
    }
}

#pragma mark - MainMenuElementViewDelegate
- (BOOL)mainMenuElementshouldAnimateClick:(MainMenuElementView*)menuItemView {
    BOOL shouldAnimate = NO;
    NSInteger viewIndex = [_menuItemViews indexOfObject:menuItemView];
    if (viewIndex != NSNotFound) {
        CGFloat idlePosition = viewIndex / (CGFloat)_menuItemViews.count;
        CGFloat selectedPosition = fmodf(1 - idlePosition, 1.0f);
        shouldAnimate = fabs(_currentPosition - selectedPosition) < 0.001;
    }
    return shouldAnimate;
}

- (void)mainMenuElementClicked:(MainMenuElementView*)menuItemView {
    NSInteger viewIndex = [_menuItemViews indexOfObject:menuItemView];
    if (viewIndex != NSNotFound) {
        CGFloat idlePosition = viewIndex / (CGFloat)_menuItemViews.count;
        CGFloat selectedPosition = fmodf(1 - idlePosition, 1.0f);
        if (fabs(_currentPosition - selectedPosition) < 0.001) {
            [_delegate mainMenuController:self didSelectMenuItem:menuItemView.menuItem];
        }
        else {
            [self setCurrentPosition:selectedPosition animated:YES];
        }
    }
}

#pragma mark - View Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //Kanchan Nair: CR#5 Added to call check for Update method in menu page
    
    VentanaObj = [[VentanaPrincipalViewController alloc]init];
    [VentanaObj performSelector:@selector(CompruebaConexion)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidDisappear:(BOOL)animated {
    [self cancelAnimations];
    [super viewDidDisappear:animated];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self reloadUI];
}

- (void)viewDidUnload {
    [self setPanGestureRecognizer:nil];
    [self setMenuContentView:nil];
    [self setMenuItemViews:nil];
    [super viewDidUnload];
}

@end
