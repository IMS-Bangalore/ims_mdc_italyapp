//
//  HelpPopoverContentViewController.m
//  IMS Digital EPM
//
//  Created by Diego Prados on 18/10/12.
//
//

#import "HelpPopoverContentViewController.h"

static const CGFloat kPopoverMaxHeight = 330;

@interface HelpPopoverContentViewController ()

@property (nonatomic, retain) NSString *helpTitle;
@property (nonatomic, retain) NSString *helpMessage;

@end

@implementation HelpPopoverContentViewController

@synthesize titleLabel = _titleLabel;
@synthesize messageLabel = _messageLabel;
@synthesize messageScrollView = _messageScrollView;
@synthesize helpTitle = _helpTitle;
@synthesize helpMessage = _helpMessage;

#pragma mark - Init
- (id)initWithTitle:(NSString *)title andMessage:(NSString*)message {
    self = [super init];
    if (self) {
        // Custom initialization
        _helpTitle = title;
        _helpMessage = message;
    }
    return self;
}

#pragma mark - Public methods
- (CGSize)sizeInPopover {
    return self.view.frame.size;
}

#pragma mark - View lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.contentSizeForViewInPopover = self.view.frame.size;
    
    //Kanchan: Added to put background for the popup for ios 8 version
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"dialogo_selec_fechavisita.png"]];
}

- (void)viewDidAppear:(BOOL)animated {
    [self.messageScrollView flashScrollIndicators];
}

- (void)viewWillAppear:(BOOL)animated {
    _titleLabel.text = _helpTitle;
    _messageLabel.text = _helpMessage;
    [_messageLabel sizeToFit];
    
    if (self.messageLabel.frame.size.height > self.messageScrollView.frame.size.height || self.messageLabel.frame.size.height < self.messageScrollView.frame.size.height) {
        [self calculateNewHeigth];
    }
    self.messageScrollView.contentSize = CGSizeMake(self.messageLabel.frame.size.width,self.messageLabel.frame.size.height);
    self.contentSizeForViewInPopover = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height);
    [super viewWillAppear:animated];
}

- (void)calculateNewHeigth {
    if (self.messageLabel.frame.size.height < kPopoverMaxHeight) {
        CGFloat newHeight = self.view.frame.size.height + (self.messageLabel.frame.size.height - self.messageScrollView.frame.size.height);
        self.messageScrollView.frame = CGRectMake(self.messageScrollView.frame.origin.x, self.messageScrollView.frame.origin.y, self.messageScrollView.frame.size.width, self.messageLabel.frame.size.height);
        self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, newHeight);
    }
}

- (void)viewDidUnload {
    [self setTitleLabel:nil];
    [self setMessageLabel:nil];
    [self setMessageScrollView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	return YES;
}

- (void)dealloc {
    [_titleLabel release];
    [_messageLabel release];
    [_messageScrollView release];
    [super dealloc];
}
@end
