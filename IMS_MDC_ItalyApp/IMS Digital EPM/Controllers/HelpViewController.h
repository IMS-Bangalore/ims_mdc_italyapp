//
//  HelpViewController.h
//  IMS Digital EPM
//
//  Created by Diego Prados on 17/10/12.
//
//

#import <UIKit/UIKit.h>

@protocol HelpViewControllerDelegate <NSObject>

- (BOOL)helpViewControllerShouldShow;
- (void)helpViewControllerDidChangePosition:(CGFloat)position animated:(BOOL)animated;

@end

@interface HelpViewController : UIViewController <UIPopoverControllerDelegate, UIGestureRecognizerDelegate>

@property (assign, nonatomic) id<HelpViewControllerDelegate> delegate;
@property (retain, nonatomic) IBOutlet UIButton *showHelpButton;
@property (retain, nonatomic) IBOutlet UIImageView *helpImageView;
@property (retain, nonatomic) IBOutlet UIView *offsetPlaceholderView;
@property (retain, nonatomic) IBOutlet UIButton *invisibleShowHelpButton;
@property (retain, nonatomic) IBOutletCollection(UIButton) NSArray *actionButtons;
@property (retain, nonatomic) IBOutlet UIView *containerView;
@property (retain, nonatomic) IBOutlet UIImageView *helpButtonArrowImageView;
@property (retain, nonatomic) IBOutlet UIView *helpMessageView;
@property (retain, nonatomic) IBOutlet UILabel *helpMessageLabel;
@property (retain, nonatomic) IBOutlet UIView *shadowHelpMessageView;
@property (retain, nonatomic) IBOutlet UILabel *helpMessageTitleLabel;

- (CGFloat)viewOffset;
- (IBAction)showHelp:(id)sender;
- (IBAction)dismissHelpMessage:(id)sender;

@end
