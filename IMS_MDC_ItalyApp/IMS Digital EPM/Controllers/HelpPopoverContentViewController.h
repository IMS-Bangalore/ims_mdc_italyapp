//
//  HelpPopoverContentViewController.h
//  IMS Digital EPM
//
//  Created by Diego Prados on 18/10/12.
//
//

#import <UIKit/UIKit.h>

@interface HelpPopoverContentViewController : UIViewController

@property (retain, nonatomic) IBOutlet UILabel *titleLabel;
@property (retain, nonatomic) IBOutlet UILabel *messageLabel;
@property (retain, nonatomic) IBOutlet UIScrollView *messageScrollView;

- (id)initWithTitle:(NSString *)title andMessage:(NSString*)message;
- (CGSize)sizeInPopover;

@end
