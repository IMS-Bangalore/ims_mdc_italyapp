//
//  SpecialitySelectionViewController.h
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 11/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import <UIKit/UIKit.h>


#import "Specialty.h"

@class SpecialitySelectionViewController;
@protocol SpecialitySelectionViewControllerDelegate
- (void)specialitySelectionController:(SpecialitySelectionViewController*)popup didSelectSpeciality:(id)speciality;
@end

@interface SpecialitySelectionViewController : UIViewController

@property (assign, nonatomic) id<SpecialitySelectionViewControllerDelegate> delegate;
@property (retain, nonatomic) id disabled;
@property (retain, nonatomic) NSArray* values;
@property (retain, nonatomic) NSString* windowTitle;

//Utpal: Added for demographic CR#4 Speciality - START
@property (nonatomic, retain) NSArray *tagArray;
@property (nonatomic, retain) NSArray *defaultValues;
@property (nonatomic, retain) NSString *tagIdentifierField;
@property (nonatomic, assign) id defaultSelectedValueSpeciality;
@property (nonatomic, assign) id defaultSelectedValueOtherSpeciality;

/// END
//
@property (retain, nonatomic) NSArray* otherSpecialityValues;

@property (nonatomic, retain) NSArray *tagArrayOtherSpeciality;
@property (nonatomic, retain) NSArray *defaultValuesOtherSpeciality;
@property (nonatomic, retain) NSString *tagIdentifierFieldOtherSpeciality;


@property (retain, nonatomic) IBOutlet UILabel *labelTitulo;
@property (retain, nonatomic) IBOutletCollection(UIButton) NSArray* buttons;

@end
