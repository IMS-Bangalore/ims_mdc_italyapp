//
//  NotificationsViewController.m
//  IMS Digital EPM
//
//  Created by Diego Prados on 05/12/12.
//
//

#import "TwinCodersLibrary.h"
#import "MultipleChoiceViewController.h"
#import "NotificationsViewController.h"
#import "RESTJSONRequest.h"
#import "RequestFactory.h"
#import "NotificationCell.h"
#import "Notification.h"
#import "Device.h"

static NSString* const kNotificationCellIdentifier = @"notificationCell";
static NSString* const kNotificationCellNib = @"NotificationCell";
static NSString* const kHeaderFieldName = @"Authorization";
static NSString* const kHeaderFieldValue = @"Basic dHdpbnM6YTRiZDQyMDZkYzYzNzQyYjM2Mzc3OTZhOTE2MjVhYjc=";

@interface NotificationsViewController ()

@property (nonatomic, retain) MultipleChoiceViewController* choiceController;
@property (nonatomic, retain) RequestFactory* requestFactory;
@property (nonatomic, retain) TCBaseRequest* request;
@property (nonatomic, retain) NSMutableURLRequest *requestObj;
@property (nonatomic, retain) Device* usedDevice;
@property (nonatomic, retain) NSNumber* selectedNotificationId;
@property (nonatomic) BOOL loading;

@end

@implementation NotificationsViewController

@synthesize notifications = _notifications;
@synthesize pushToken = _pushToken;
@synthesize userId = _userId;

#pragma mark - Init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    self.webView.delegate = self;
    self.webView.alpha = 0;
    
//    Dummy
//    Notification* notification = [[Notification alloc] init];
//    notification.description = @"Descripción de prueba de la nueva seccion de notificaciones";
//    notification.contentUrl = @"http://www.twincoders.com";
//    notification.date = [NSDate date];
//    Notification* notification2 = [[Notification alloc] init];
//    notification2.description = @"Otra prueba más";
//    notification2.contentUrl = @"http://www.apple.com";
//    notification2.date = [NSDate date];
//
//    Notification* notification3 = [[Notification alloc] init];
//    notification3.description = @"La última prueba";
//    notification3.contentUrl = @"http://www.lumata.com";
//    notification3.date = [NSDate date];
//
//    self.notifications = [NSArray arrayWithObjects:notification, notification2, notification3, nil];
//    [self.tableView reloadData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // If no notification selected, select the first
    if (self.selectedNotificationId == nil && self.notifications.count > 0) {
        [self selectNotification:self.notifications[0]];
    }
    
    [self sendRequestShowingErrors:YES];
    [self.tableView flashScrollIndicators];
    
//Ravi_Bukka: Added for localization
    _mailBoxLabel.text = NSLocalizedString(@"MAILBOX_LABEL", nil);
    _messagesLabel.text = NSLocalizedString(@"MESSAGE_LABEL", nil);
    
}

- (void)viewWillDisappear:(BOOL)animated {
    if (_choiceController != nil) {
        [_delegate notificationsInboxWillDisappear];
        [_delegate dismissModalController:_choiceController];
        _choiceController = nil;
    }
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_tableView release];
    [_webView release];
    [super dealloc];
}

- (void)viewDidUnload {
    [self setTableView:nil];
    [self setWebView:nil];
    [super viewDidUnload];
}

#pragma mark - Private methods

- (void)sendGetNofiticationsRequestShowingErrors:(BOOL)showErrors {
    self.request = [self.requestFactory createGetDeviceNotificationsRequestWithDevice:_usedDevice onComplete:^(NSArray *array) {
        self.notifications = [NSArray arrayWithArray:array];
        NSIndexPath* selectedIndexPath = nil;
        
        if (_selectedNotificationId != nil) {
            int position = 0;
            for (Notification* notification in _notifications) {
                if ([notification.notificationId isEqual:_selectedNotificationId]) {
                    selectedIndexPath = [NSIndexPath indexPathForRow:position inSection:0];
                }
                position++;
            }
        }
        
        [_tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
        if (selectedIndexPath != nil && selectedIndexPath.row < self.notifications.count) {
            [_tableView selectRowAtIndexPath:selectedIndexPath animated:NO scrollPosition:UITableViewRowAnimationNone];
        }
        
        // If no notification selected, select the first
        if (self.selectedNotificationId == nil && self.notifications.count > 0) {
            [self selectNotification:self.notifications[0]];
        }
        
        [self.delegate notificationsControllerDidFinishLoading:self];
    } onError:^(NSError *error) {
        if (showErrors) {
            [self displayAlert:error.localizedDescription];
        }
    }];
    [self.request start];
}

//Ravi_Bukka: Here we are going to call the APNS register device service
- (void)sendRequestShowingErrors:(BOOL)showErrors {
    if (self.usedDevice == nil) {
        self.requestFactory = [RequestFactory sharedInstance];
        self.request = [self.requestFactory createCreateDeviceRequestWithToken:self.pushToken andDeviceAlias:_userId onComplete:^(Device *device) {
            if (device != nil) {
                self.usedDevice = device;
                [self sendGetNofiticationsRequestShowingErrors:showErrors];
                
                NSLog(@"RRRAAAVVIIII working coming in onComplete");
            }
        } onError:^(NSError *error) {
            if (showErrors) {
                [self displayAlert:error.localizedDescription];
            }
        }];
        [self.request start];
    } else {
        [self sendGetNofiticationsRequestShowingErrors:showErrors];
    }
}

- (void)displayAlert:(NSString*)message {
    if (self.choiceController != nil) {
        [_delegate dismissModalController:self.choiceController];
    }
    self.choiceController = [[MultipleChoiceViewController new] autorelease];
    _choiceController.message = message;
    _choiceController.messageFont = [UIFont boldSystemFontOfSize:32];
    _choiceController.acceptButtonTitle = nil;
    _choiceController.cancelButtonTitle = NSLocalizedString(@"CHANGE_PASSWORD_ERROR_BUTTON", @"");
    _choiceController.otherButtonTitle = nil;
    _choiceController.eventBlock = ^(MultipleChoiceButton button){
        // Dismiss and release the controller
        [_delegate dismissModalController:self.choiceController];
        self.choiceController = nil;
    };
    
    // Present modal controller
    [_delegate presentModalController:self.choiceController];
}

#pragma mark - Public methods

- (void)loadNotifications {
    [self sendRequestShowingErrors:NO];
}

- (NSInteger)numberOfUnreadNotificationsForDate:(NSDate*)lastDate {
    int numberOfUnreadNotifications = 0;
    if (lastDate == nil) {
        numberOfUnreadNotifications = self.notifications.count;
    } else {
        for (Notification* notification in self.notifications) {
            if ([notification.date compare:lastDate] == NSOrderedDescending) {
                numberOfUnreadNotifications++;
            }
        }
    }
    return numberOfUnreadNotifications;
}

- (void)selectNotification:(Notification*)notification {
    if ([self isViewLoaded]) {
        NSURL *url = [NSURL URLWithString:notification.contentUrl];
        self.selectedNotificationId = notification.notificationId;
        
        if (![self.webView.request.URL isEqual:url]) {
            [UIView animateWithDuration:0.5 animations:^{
                self.webView.alpha = 0;
            }];
            self.webView.hidden = YES;
            
            // Load empty URL to show blank webview
            self.requestObj = [NSURLRequest requestWithURL:[NSURL URLWithString:@""]];
            [self.webView loadRequest:_requestObj];
            
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setValue:kHeaderFieldValue forHTTPHeaderField:kHeaderFieldName];
            [self.webView loadRequest:request];
        }
        
        NSInteger row = [_notifications indexOfObject:notification];
        if (row != NSNotFound) {
            [_tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:row inSection:0] animated:YES scrollPosition:UITableViewScrollPositionMiddle];
        }
    }
}

#pragma mark - UITableViewDelegate
- (BOOL)hasResults {
    return self.notifications.count > 0;
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.notifications.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = nil;
    if ([self hasResults]) {
        NotificationCell* notificationCell = nil;
        notificationCell = [tableView dequeueReusableCellWithIdentifier:kNotificationCellIdentifier];
        if (notificationCell == nil) {
            notificationCell = [NotificationCell loadFromNIB:kNotificationCellNib];
        }
        
        if (self.selectedNotificationId != nil) {
            NSIndexPath* selectedIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
            int position = 0;
            for (Notification* notification in _notifications) {
                if ([notification.notificationId isEqual:_selectedNotificationId]) {
                    selectedIndexPath = [NSIndexPath indexPathForRow:position inSection:0];
                    break;
                }
                position++;
            }
            BOOL loading = self.loading && [selectedIndexPath isEqual:indexPath];
            [notificationCell setLoading:loading];
        }
        
        notificationCell.notification = self.notifications[indexPath.row];
        cell = notificationCell;
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return tableView.rowHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self selectNotification:self.notifications[indexPath.row]];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([tableView.indexPathsForVisibleRows containsObject:indexPath]) {
        NotificationCell* selectedCell = (NotificationCell*)[self.tableView cellForRowAtIndexPath:indexPath];
        [selectedCell setLoading:NO];
    }
}

#pragma mark - UIWebViewDelegate

- (void)webViewDidStartLoad:(UIWebView *)webView {
    self.webView.hidden = NO;
    [UIView animateWithDuration:0.5 animations:^{
        self.webView.alpha = 1;
    }];
    [(NotificationCell*)[self.tableView cellForRowAtIndexPath:self.tableView.indexPathForSelectedRow] setLoading:YES];
    self.loading = YES;

}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [(NotificationCell*)[self.tableView cellForRowAtIndexPath:self.tableView.indexPathForSelectedRow] setLoading:NO];
    self.loading = NO;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    DLog(@"Error: %d", error.code);
    if (error.code != -999) {
        [self displayAlert:NSLocalizedString(@"NOTIFICATIONS_ERROR", @"")];
    }
    [(NotificationCell*)[self.tableView cellForRowAtIndexPath:self.tableView.indexPathForSelectedRow] setLoading:NO];
    self.loading = NO;
}

@end
