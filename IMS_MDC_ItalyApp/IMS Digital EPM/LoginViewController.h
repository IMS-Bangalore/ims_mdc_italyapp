//
//  LoginViewController.h
//  IMS Digital EPM
//
//  Created by Ricardo Berzal on 23/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Doctor.h"
#import "DoctorInfo.h"
#import "ModalControllerProtocol.h"

@protocol LoginViewControllerDelegate <ModalControllerProtocol>
- (void)didLoginWithUser:(Doctor *)doctor firstTime:(BOOL)firstTime;
- (void)setIsInCollaborationPeriod:(BOOL)inCollaborationPeriod;

//Ravi_Bukka: CR #3 Extending 1 week of grace period based on doctor request and server response
- (void)setNeedToExtendGracePeriod:(BOOL)extendGracePeriod;
@end

typedef enum {LOGIN,CAMBIOCONTRASENA} tVentanaActiva;

extern const NSInteger kDivideToObtainDays;
extern const NSInteger kMaxDayAfterCollaborationEnds;

//Ravi_Bukka: CR #3 Extending 1 week of grace period based on doctor request and server response
extern const NSInteger kMaxDayAfterExtendCollaborationEnds;

@interface LoginViewController : UIViewController <UITextFieldDelegate> {
    id<LoginViewControllerDelegate> _delegate;
    
    tVentanaActiva _EstadoVentana;
    BOOL _TecladoVisible;
    BOOL _checkingLogin;
    
    //Deepak_Carpenter : Added to handle CollaborationDates
    NSDate* firstCollaborationDate;
    NSDate* firstCollaborationDateEnd;
    NSDate* secondCollaborationDate;
    NSDate* secondCollaborationDateEnd;
    NSDate* thirdCollaborationDate;
    NSDate* thirdCollaborationDateEnd;
    NSDate* fourthCollaborationDate;
    NSDate* fourthCollaborationDateEnd;
    NSDate* fifthCollaborationDate;
    NSDate* fifthCollaborationDateEnd;
    NSDate *dateIncremented;  //Utpal CR#7 for Data Retention
    //Utpal CR#4 for Demographics
    NSNumber *age;
    NSNumber* gender;
    NSNumber *specialty;
    NSNumber *Otherspecialty;
    NSString *email;
    NSNumber *patientsPerWeek;
    NSString *comments;
    
    
    
    int collaboration;
    
    //Ravi_Bukka: CR #3 Extending 1 week of grace period based on doctor request and server response
    int toExtendGracePeriodFirstWeek;
    int toExtendGracePeriodSecondWeek;
    int toExtendGracePeriodThirdWeek;
    
}

@property (retain, nonatomic) IBOutlet UIView *VistaLogin;
@property (retain, nonatomic) IBOutlet UIView *VistaCambiarContrasena;


@property (retain, nonatomic) IBOutlet UITextField *TextUsuario;
@property (retain, nonatomic) IBOutlet UITextField *TextContrasena;
@property (retain, nonatomic) IBOutlet UITextField *TextContrasenaActual;
@property (retain, nonatomic) IBOutlet UITextField *TextNuevaContrasena;
@property (retain, nonatomic) IBOutlet UITextField *TextConfirmeNuevaContrasena;

@property (retain, nonatomic) IBOutlet UIButton *BotonAyuda;
@property (retain, nonatomic) IBOutlet UIButton *BotonIrACambiarContrasena;
@property (retain, nonatomic) IBOutlet UIButton *BotonCambiarContrasena;
@property (retain, nonatomic) IBOutlet UIButton *BotonComprobarLogin;

@property (retain, nonatomic) IBOutlet UILabel *appVersionLabel;

- (IBAction)BotonIrACambiarContrasenaTouchUpInside:(id)sender;
- (IBAction)hidePasswordChange;
- (IBAction)loginButtonClicked;
- (IBAction)forgotPasswordButtonClicked:(id)sender;
- (IBAction)changePasswordButtonClicked:(id)sender;

- (id)initWithDelegate:(id<LoginViewControllerDelegate>) delegate;

- (void)keyboardWillHide:(NSNotification*)aNotification;


@end
