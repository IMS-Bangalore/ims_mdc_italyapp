//
//  ViewController.h
//  IMS Deactivated
//
//  Created by Guillermo Gutiérrez on 22/03/13.
//
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

#pragma mark - IBAction
- (IBAction)downloadButtonTapped:(id)sender;

@end
